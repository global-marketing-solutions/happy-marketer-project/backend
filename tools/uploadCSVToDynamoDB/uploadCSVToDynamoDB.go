package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"regexp"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	path = "reports/first-real-dummy.csv"

	tableName = "marketegy"

	maxItemInBatch = 25
)

var (
	floatCrazy = regexp.MustCompile(`\d+\.\d{3,}`)

	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Profile: "gms-ai",
		Config: aws.Config{
			Region: aws.String("us-east-1"),
		},
	}))
	dynamoClient = dynamodb.New(sess)

	statsKeys = map[string]bool{}
)

func main() {
	r := csv.NewReader(strings.NewReader(getCSV()))
	all, err := r.ReadAll()
	if err == io.EOF {
		panic(err)
	}
	fields, records := all[0], all[1:]
	normalizeFields(&fields)
	normalizeRecords(fields, &records)

	for i := 0; i < len(records)-1; {
		to := i + maxItemInBatch
		if to > len(records)-1 {
			to = len(records) - 1
		}
		fmt.Printf("i: %v, to: %v\n", i, to)

		input := &dynamodb.BatchWriteItemInput{}
		addRecordsToBatch(input, fields, records[i:to])
		i = to
		_, err := dynamoClient.BatchWriteItem(input)
		if err != nil {
			fmt.Println("input:", input)
			panic(err)
		}
	}
	fmt.Printf(
		"Deployed items (if less then number of rows in CSV - you have duplikated keys): %v\n",
		len(statsKeys),
	)
}

func getCSV() string {
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return string(dat)
}

func normalizeFields(fields *[]string) {
	for i, field := range *fields {
		switch field {
		case "GROUP":
			(*fields)[i] = "group"
		case "BRAND":
			(*fields)[i] = "brand"
		case "Spin speed":
			(*fields)[i] = "rpm"
		case "Energy consumption":
			(*fields)[i] = "energy"
		case "Price, unit":
			(*fields)[i] = "price"
		case "Wgt. Distribution":
			(*fields)[i] = "distribution"
		default:
			(*fields)[i] = strings.Trim(field, " ")
		}
	}
	*fields = append(*fields, "pk")
	*fields = append(*fields, "sk")
}
func normalizeRecords(fields []string, records *[][]string) {

	for indexOfRow, record := range *records {
		(*records)[indexOfRow] = append((*records)[indexOfRow], "report gms-ai")
		(*records)[indexOfRow] = append((*records)[indexOfRow], unixNano())

		if len(fields) != len((*records)[indexOfRow]) {
			fmt.Printf("len of fields: %v, len of record: %v\n", len(fields), len(record))
			panic(fmt.Sprintf("fields: %v\nrecord: %v", fields, record))
		}

		for indexOfRecordValue, val := range record {
			switch {
			case floatCrazy.MatchString(val):
				i := strings.Index(val, ".")
				(*records)[indexOfRow][indexOfRecordValue] = val[:i]
			default:
				(*records)[indexOfRow][indexOfRecordValue] = strings.ToLower(strings.TrimSpace(val))
			}
		}
	}

}

func unixNano() string {
	n := fmt.Sprint(time.Now().UnixNano())
	return n[0 : len(n)-3]
}

func addRecordsToBatch(batchWriteItemInput *dynamodb.BatchWriteItemInput, fields []string, records [][]string) {
	writeRequests := []*dynamodb.WriteRequest{}

	for _, record := range records {
		putRequest := buildPutRequest(fields, record)
		writeRequests = append(writeRequests, &dynamodb.WriteRequest{
			PutRequest: putRequest,
		})
	}
	// https://docs.aws.amazon.com/sdk-for-go/api/service/dynamodb/#BatchWriteItemInput.SetRequestItems
	batchWriteItemInput.SetRequestItems(
		map[string][]*dynamodb.WriteRequest{
			tableName: writeRequests,
		},
	)

}

func buildPutRequest(fields, record []string) *dynamodb.PutRequest {
	item := map[string]*dynamodb.AttributeValue{}

	for i, r := range record {
		field := strings.ToLower(fields[i])

		if isNumber(field) {
			item[field] = &dynamodb.AttributeValue{
				N: aws.String(r),
			}
		} else {
			item[field] = &dynamodb.AttributeValue{
				S: aws.String(r),
			}
		}

		if field == "sk" {
			statsKeys[r] = true
		}
	}

	return &dynamodb.PutRequest{
		Item: item,
	}
}

func isNumber(field string) bool {
	fieldsAsNumber := map[string]bool{
		"distribution": true,
		"price":        true,
		"rpm":          true,
		"speed":        true,
		"tos":          true,
		"units":        true,
		"value":        true,
	}
	return fieldsAsNumber[field]
}
