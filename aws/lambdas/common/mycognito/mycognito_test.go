package mycognito

import (
	"testing"
)

func init() {
	InitWithoutSession()
}

func TestGetHumanName(t *testing.T) {
	t.Run("firstRun", func(t *testing.T) {
		name := GetHumanName("vitaly@gms-ai.com")
		if name != "Vitaly Zdanevich" {
			t.Fatalf("GetHumanName(): got %v", name)
		}
	})
	t.Run("secondRunTheSameEmail", func(t *testing.T) {
		name := GetHumanName("vitaly@gms-ai.com")
		if name != "Vitaly Zdanevich" {
			t.Fatalf("GetHumanName(): got %v", name)
		}
	})
	t.Run("anotherEmail", func(t *testing.T) {
		name := GetHumanName("vitaliy.sichkar@gms-ai.com")
		if name != "Vitaliy Sichkar" {
			t.Fatalf("GetHumanName(): got %v", name)
		}
	})
}

func TestIsUserExists(t *testing.T) {
	t.Run("yes", func(t *testing.T) {
		b := IsUserExists("vitaly@gms-ai.com")
		if !b {
			t.Fatal()
		}
	})
	t.Run("no", func(t *testing.T) {
		b := IsUserExists("i-am-not-exists@xxx.com")
		if b {
			t.Fatal()
		}
	})

}
