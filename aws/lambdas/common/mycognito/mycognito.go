package mycognito

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

var (
	Cogn *cognitoidentityprovider.CognitoIdentityProvider
	Sess *session.Session

	loginsToHumanNamesCache map[string]string

	adminGetUserInput = cognitoidentityprovider.AdminGetUserInput{
		UserPoolId: aws.String("us-east-1_DEl9xiAue"),
	}
)

func InitWithSession(sess *session.Session) {
	Cogn = cognitoidentityprovider.New(sess)
}

func InitWithoutSession() {
	Sess = session.Must(session.NewSession())
	Cogn = cognitoidentityprovider.New(Sess)
}

func GetHumanName(email string) string {
	if loginsToHumanNamesCache == nil {
		loginsToHumanNamesCache = make(map[string]string)
	}

	if name, ok := loginsToHumanNamesCache[email]; ok {
		return name
	}

	adminGetUserInput.SetUsername(email)
	adminGetUserOutput, err := Cogn.AdminGetUser(&adminGetUserInput)
	if err != nil {
		panic(err)
	}

	for _, userAttribute := range adminGetUserOutput.UserAttributes {
		if *userAttribute.Name == "name" {
			name := *userAttribute.Value
			loginsToHumanNamesCache[email] = name
			return name
		}
	}

	panic("Not found name for email: " + email)
}

func IsUserExists(email string) bool {
	fmt.Printf("IsUserExists(): email: %v\n", email)

	out, err := Cogn.ListUsers(&cognitoidentityprovider.ListUsersInput{
		UserPoolId:      aws.String("us-east-1_DEl9xiAue"),
		Filter:          aws.String("email = \"" + email + "\""),
		AttributesToGet: []*string{aws.String("email")},
	})
	if err != nil {
		panic(err)
	}
	if len(out.Users) == 1 {
		fmt.Println("User does exist")
		return true
	}
	fmt.Println("User does not exist")
	return false
}
