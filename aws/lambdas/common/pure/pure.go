package pure

import (
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

func BuildSortKey(email, projectName string) string {
	at := strings.Index(email, "@")
	user := email[0:at]
	domain := email[at+1:]
	return domain + " " + user + " " + projectName
}

func BuildSortKeyForOwner(email, owner, project string) string {
	return ExtractDomain(email) + " " + owner + " " + project
}

func ExtractUsername(email string) string {
	at := strings.Index(email, "@")
	return email[:at]
}

func Username(sk string) string {
	split := strings.Split(sk, " ")
	return split[1]
}

func ExtractDomain(email string) string {
	at := strings.Index(email, "@")
	return email[at+1:]
}

func Subdomain(email string) string {
	return regexp.MustCompile(`@(.+?)\.`).FindStringSubmatch(email)[1]
}

func StrToInt(s string) int {
	n, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		panic(err)
	}
	return int(n)
}

func StrToFloat64(s string) float64 {
	n, err := strconv.ParseFloat(s, 64)
	if err != nil {
		panic(err)
	}
	return n
}

func DecodePercent(s string) string {
	decoded, err := url.QueryUnescape(s)
	if err != nil {
		panic(err)
	}
	return decoded
}

func Unpack(m map[string]string) (string, string) {
	for k, v := range m {
		return k, v
	}
	panic("Empty map")
}

func Email(sk string) string {
	split := strings.Split(sk, " ")
	return split[1] + "@" + split[0]
}

func ProjectName(sk string) string {
	split := strings.Split(sk, " ")
	return strings.Join(split[2:], " ")
}
