package pure

import (
	"fmt"
	"testing"
)

func TestBuildSortKey(t *testing.T) {
	sk := BuildSortKey("vitaly@gms-ai.com", "My project name")
	if sk != "gms-ai.com vitaly My project name" {
		t.Error("Actual sk: " + sk)
	}
	sk = BuildSortKey("john.rossman@electrolux.com", "New front loaders")
	if sk != "electrolux.com john.rossman New front loaders" {
		t.Error("Actual sk: " + sk)
	}
}

func TestExtractUsername(t *testing.T) {
	t.Run("string", func(t *testing.T) {
		username := ExtractUsername("vitaly@gms-ai.com")
		if username != "vitaly" {
			t.Fatalf("Got: %v\n", username)
		}
	})

	t.Run("string", func(t *testing.T) {
		username := ExtractUsername("jonh.smith@electrolux.co.uk")
		if username != "jonh.smith" {
			t.Fatalf("Got: %v\n", username)
		}
	})

}

func TestExtractDomain(t *testing.T) {
	domain := ExtractDomain("vitaly@gms-ai.com")
	if domain != "gms-ai.com" {
		t.Errorf("ExtractDomain(): got %v\n", domain)
	}
}

func TestSubdomain(t *testing.T) {
	subdomain := Subdomain("vitaly@gms-ai.com")
	if subdomain != "gms-ai" {
		t.Errorf("Subdomain(): got %v\n", subdomain)
	}

	subdomain = Subdomain("aaa.bbb@zzz.com")
	if subdomain != "zzz" {
		t.Errorf("Subdomain(): got %v\n", subdomain)
	}
}

func TestStrToInt(t *testing.T) {
	t.Run("decimal", func(t *testing.T) {
		f := StrToInt("1")
		if f != 1 {
			t.Fatalf("Got: %v\n", f)
		}
	})
	t.Run("decimalAnother", func(t *testing.T) {
		f := StrToInt("10")
		if f != 10 {
			t.Fatalf("Got: %v\n", f)
		}
	})
}

func TestStrToFloat64(t *testing.T) {
	t.Run("decimal", func(t *testing.T) {
		f := StrToFloat64("10")
		if f != float64(10) {
			t.Fatalf("Got: %f\n", f)
		}
	})
	t.Run("float", func(t *testing.T) {
		f := StrToFloat64("10.5")
		if f != float64(10.5) {
			t.Fatalf("Got: %f\n", f)
		}
	})
}

func TestDecodePercent(t *testing.T) {
	t.Run("string", func(t *testing.T) {
		decoded := DecodePercent("%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9")
		if decoded != "Русский" {
			t.Fatalf("Got: %v\n", decoded)
		}
	})

	t.Run("string-with-space", func(t *testing.T) {
		decoded := DecodePercent("%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9%20%D1%81%20%D0%BF%D1%80%D0%BE%D0%B1%D0%B5%D0%BB%D0%BE%D0%BC")
		if decoded != "Русский с пробелом" {
			t.Fatalf("Got: %v\n", decoded)
		}
	})
}

func TestUnpack(t *testing.T) {
	k, v := Unpack(map[string]string{"a": "b"})
	fmt.Printf("k: %v, v: %v\n", k, v)
	if k != "a" || v != "b" {
		t.Fatal()
	}
}

func TestEmail(t *testing.T) {
	email := Email("gms-ai.com vitaly My project")
	if email != "vitaly@gms-ai.com" {
		t.Fatalf("getEmail(): got %v", email)
	}
}

func TestProjectName(t *testing.T) {
	name := ProjectName("gms-ai.com vitaly My project")
	if name != "My project" {
		t.Fatalf("getProjectName(): got %v", name)
	}
}
