package mydynamo

import (
	"fmt"

	"../pure"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

const (
	TableName = "marketegy"
)

var (
	Sess           = session.Must(session.NewSession())
	DynamodbClient = dynamodb.New(Sess)
)

func AddProject(email, name, startDate, dueDate string) {
	putItemInput := dynamodb.PutItemInput{
		TableName: aws.String(TableName),
		Item: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
			"sk": {
				S: aws.String(pure.BuildSortKey(email, name)),
			},
			"startDate": {
				S: &startDate,
			},
			"dueDate": {
				S: &dueDate,
			},
			"entityStatus": {
				S: aws.String("In Progress"),
			},
		},
		ConditionExpression: aws.String("attribute_not_exists(sk)"),
	}
	resp, err := DynamodbClient.PutItem(&putItemInput)
	if err != nil {
		panic(err)
	}
	fmt.Println("AddProject() resp:", resp)
}

func PutItem(item map[string]*dynamodb.AttributeValue) {
	putItemInput := &dynamodb.PutItemInput{
		Item:      item,
		TableName: aws.String(TableName),
	}

	_, err := DynamodbClient.PutItem(putItemInput)
	if err != nil {
		panic(err)
	}
}

func UpdateItem(updateItemInput *dynamodb.UpdateItemInput) {
	out, err := DynamodbClient.UpdateItem(updateItemInput)
	fmt.Println("out:", out)
	if err != nil {
		panic(err)
	}
}

func UpdateItemIfExists(updateItemInput *dynamodb.UpdateItemInput) {
	updateItemInput.ReturnValues = aws.String("ALL_OLD")
	out, err := DynamodbClient.UpdateItem(updateItemInput)
	if err != nil {
		panic(err)
	}
	fmt.Printf("UpdateItemOutput: %#v\n", out)

	if len(out.Attributes) == 0 {
		fmt.Println("HACKING attempt")
		_, err := DynamodbClient.DeleteItem(&dynamodb.DeleteItemInput{
			TableName: updateItemInput.TableName,
			Key:       updateItemInput.Key,
		})
		if err != nil {
			panic(err)
		}
	}
}

func UpdateItemIfSharedEdit(updateItemInput *dynamodb.UpdateItemInput, username string) {
	getItemOutput, err := DynamodbClient.GetItem(&dynamodb.GetItemInput{
		TableName: updateItemInput.TableName,
		Key:       updateItemInput.Key,
	})
	if err != nil {
		panic(err)
	}
	if getItemOutput.Item["shrd"] == nil ||
		(getItemOutput.Item["shrd"] != nil && getItemOutput.Item["shrd"].M[username] == nil) ||
		(getItemOutput.Item["shrd"] != nil && *getItemOutput.Item["shrd"].M[username].S == "view") {
		fmt.Println("HACKING attempt")
		return
	}
	_, err = DynamodbClient.UpdateItem(updateItemInput)
	if err != nil {
		panic(err)
	}
}

func GetProject(sk string) map[string]*dynamodb.AttributeValue {
	getItemInput := dynamodb.GetItemInput{
		TableName: aws.String(TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
			"sk": {
				S: &sk,
			},
		},
	}
	getItemOutput, err := DynamodbClient.GetItem(&getItemInput)
	if err != nil {
		panic(err)
	}
	return getItemOutput.Item
}

func ProjectAttributeSet(email, owner, projectName, name string, value interface{}) {
	update := expression.Set(
		expression.Name("entityStatus"),
		expression.Value("In Progress"),
	).Set(
		expression.Name(name),
		expression.Value(value),
	)

	condition := expression.Equal(
		expression.Name("sk"),
		expression.Value(pure.BuildSortKey(email, projectName)),
	).Or(
		expression.Contains(
			// TODO do not use expression builder for map keys with dot, because generated code will be:
			// "(#0 = :0) OR (contains (#1.#2.#3, :1))"
			// see https://stackoverflow.com/questions/61022482/is-it-possible-to-set-to-map-when-key-contains-dot
			expression.Name("shrd."+pure.ExtractUsername(email)),
			"edit",
		),
	)

	expr, err := expression.NewBuilder().WithUpdate(update).WithCondition(condition).Build()
	if err != nil {
		panic(err)
	}

	sk := pure.BuildSortKeyForOwner(email, owner, projectName)
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName: aws.String(TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
			"sk": {
				S: &sk,
			},
		},
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
	}
	fmt.Printf("updateItemInput: %#v\n", updateItemInput)

	_, err = DynamodbClient.UpdateItem(updateItemInput)
	if err != nil {
		ErrCheck(err)
	}
}

func EntityShare(_type, sk, usernameOfSharee, mode string, isShared bool) {
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName: aws.String(TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String(_type),
			},
			"sk": {
				S: &sk,
			},
		},
	}

	expressionAttributeValue := Marshal(map[string]string{usernameOfSharee: mode})

	if isShared {
		updateItemInput.UpdateExpression = aws.String("SET shrd.#0 = :mode")
		updateItemInput.ExpressionAttributeNames = map[string]*string{
			"#0": &usernameOfSharee,
		}
		updateItemInput.ExpressionAttributeValues = map[string]*dynamodb.AttributeValue{
			":mode": &dynamodb.AttributeValue{
				S: &mode,
			},
		}
	} else {
		updateItemInput.UpdateExpression = aws.String("SET shrd = :shrd")
		updateItemInput.ExpressionAttributeValues = map[string]*dynamodb.AttributeValue{
			":shrd": expressionAttributeValue,
		}
	}

	fmt.Printf("updateItemInput: %#v\n", updateItemInput)

	UpdateItem(updateItemInput)
}

func ProjectUnshare(sk, userNameUnshare string) {
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName: aws.String(TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
			"sk": {
				S: &sk,
			},
		},
		UpdateExpression: aws.String("REMOVE shrd.#0"),
		ExpressionAttributeNames: map[string]*string{
			"#0": &userNameUnshare,
		},
	}
	fmt.Printf("updateItemInput: %#v\n", updateItemInput)

	UpdateItem(updateItemInput)
}

// Complete is not the DynamoDB expression builder because email can contain dot
// https://stackoverflow.com/questions/61022482/is-it-possible-to-set-to-map-when-key-contains-dot
func Complete(email, owner, name string) {
	sk := pure.BuildSortKeyForOwner(email, owner, name)

	updateItemInput := dynamodb.UpdateItemInput{
		TableName: aws.String(TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
			"sk": &dynamodb.AttributeValue{
				S: &sk,
			},
		},
		UpdateExpression: aws.String("SET entityStatus = :Completed"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":Completed": {
				S: aws.String("Completed"),
			},
			":sk": {
				S: aws.String(pure.BuildSortKey(email, name)),
			},
			":mode": {
				S: aws.String("edit"),
			},
		},
		ConditionExpression: aws.String(ConditionExpression()),
		ExpressionAttributeNames: map[string]*string{
			"#username": aws.String(pure.ExtractUsername(email)),
		},
	}
	fmt.Printf("updateItemInput: %#v\n", updateItemInput)
	_, err := DynamodbClient.UpdateItem(&updateItemInput)
	if err != nil {
		ErrCheck(err)
	}
}

func EntityDelete(_type, email, name string) {
	deleteItemInput := dynamodb.DeleteItemInput{
		TableName: aws.String(TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String(_type),
			},
			"sk": {
				S: aws.String(pure.BuildSortKey(email, name)),
			},
		},
	}
	fmt.Printf("deleteItemInput: %#v\n", deleteItemInput)
	resp, err := DynamodbClient.DeleteItem(&deleteItemInput)
	fmt.Printf("EntityDelete(%#v, %#v) resp: %v\n", email, name, resp)
	if err != nil {
		panic(err)
	}
}

func Marshal(v interface{}) *dynamodb.AttributeValue {
	expressionAttributeValue, err := dynamodbattribute.Marshal(v)
	if err != nil {
		panic(err)
	}
	return expressionAttributeValue
}

func UnmarshalListOfMaps(av *dynamodb.AttributeValue) []map[string]interface{} {
	out := make([]map[string]interface{}, len(av.L))
	err := dynamodbattribute.Unmarshal(av, &out)
	if err != nil {
		panic(err)
	}
	return out
}

func ConditionExpression() string {
	itemExists := "sk = :sk"
	shared := "contains(shrd.#username, :mode)"
	return itemExists + " OR " + shared
}

type Rstrs struct {
	SpecID string
	Value  interface{}
}

type ModelTheoretical struct {
	Units int `json:"-"`
	Value int `json:"-"`

	AverageSalesValue int
	Specs
}
type Model struct {
	Value float64 `json:"-"`
	Units int     `json:"-"`

	ID           string
	Brand        string
	Model        string
	Distribution string
	TOS          string
	Price        string

	Specs
}

type ModelSimple struct {
	Value float64 `json:"-"`

	Specs map[string]string
}

func NewModel(item map[string]*dynamodb.AttributeValue, specNames []string) (Model, error) {
	m := Model{
		Value:        pure.StrToFloat64(*item["value"].N),
		Units:        pure.StrToInt(*item["units"].N),
		ID:           *item["sk"].S,
		Brand:        *item["brand"].S,
		Model:        *item["model"].S,
		Distribution: *item["distribution"].N,
		TOS:          *item["tos"].N,
		Price:        *item["price"].N,
	}

	m.Specs = ItemToSpecs(item, specNames)

	if m.Specs == nil {
		return m, fmt.Errorf("No such X or Y spec in this item")
	}

	return m, nil
}

func NewModelSimple(value float64, specs []string, item map[string]*dynamodb.AttributeValue) ModelSimple {
	m := ModelSimple{Value: value}

	m.Specs = make(map[string]string, len(specs))
	for _, n := range specs {
		m.SetValue(n, item[n])
	}

	return m
}
func (m *ModelSimple) SetValue(valueName string, v *dynamodb.AttributeValue) {
	if v.S != nil {
		m.Specs[valueName] = *v.S
	} else if v.N != nil {
		m.Specs[valueName] = *v.N
	} else {
		panic("No value in AttributeValue: " + v.String())
	}
}
func (m Model) EqByXY(sp Specs, specNameX, specNameY string) bool {
	for k, v := range m.Specs {
		if k == specNameX && v != sp[k] || k == specNameY && v != sp[specNameY] {
			return false
		}
	}
	return true
}

func (m *Model) String() string {
	return fmt.Sprintf("%#v", *m)
}

func FindByAllSpecs(models []Model, m Model) (int, bool) {
outer:
	for i, model := range models {
		for name, val := range model.Specs {
			v, ok := m.Specs[name]
			if ok && v != val {
				continue outer
			}
		}
		return i, true
	}
	return -1, false
}

type Specs map[string]string

func (sp *Specs) EqByAllSpecs(specs Specs) bool {
	for k, v := range *sp {
		if specs[k] != v {
			return false
		}
	}
	return true
}

func ItemToSpecs(item map[string]*dynamodb.AttributeValue, specNames []string) Specs {
	specs := make(map[string]string, len(specNames))
	for i, specName := range specNames {
		if item[specName] == nil {
			if i < 2 {
				return nil
			}
			continue
		}
		s := item[specName].S
		if s != nil {
			specs[specName] = *s
			continue
		}
		specs[specName] = *item[specName].N
	}
	return specs
}

func RprtsQuery(cntrs []string, ctgr, sgmnt, chnl, dmn string, pr [2]uint16, rstrs []Rstrs) *dynamodb.QueryInput {
	keyCond := expression.Key("pk").Equal(expression.Value("report " + dmn))

	fltr := expression.Name("category").Equal(expression.Value(ctgr)).And(
		expression.Name("segment").Equal(expression.Value(sgmnt)),
	)

	if chnl != "all" {
		fltr = fltr.And(expression.Name("channel").Equal(expression.Value(chnl)))
	}

	var fltrCountries expression.ConditionBuilder
	for i, c := range cntrs {
		cond := expression.Name("country").Equal(expression.Value(c))
		if i == 0 {
			fltrCountries = cond
		} else {
			fltrCountries = fltrCountries.Or(cond)
		}
	}
	fltr = fltr.And(fltrCountries)

	if pr[1] != 0 {

		fltr = fltr.And(expression.Between(
			expression.Name("price"),
			expression.Value(pr[0]),
			expression.Value(pr[1]),
		))

	}

	if len(rstrs) > 0 {
		for _, r := range rstrs {
			fltr = fltr.And(expression.Name(r.SpecID).NotEqual(expression.Value(r.Value)))
		}
	}

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(fltr).Build()
	if err != nil {
		panic(err)
	}

	// https://docs.aws.amazon.com/sdk-for-go/api/service/dynamodb/#DynamoDB.QueryPages
	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String("marketegy"),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	fmt.Printf("queryInput: %#v\n", queryInput)

	return queryInput
}

func ErrCheck(err error) {
	ae, _ := err.(awserr.RequestFailure)
	if ae.Code() == "ConditionalCheckFailedException" {
		fmt.Println("HACKING attempt")
	} else {
		panic(err)
	}
}

func HasEditAccess(sk string, item map[string]*dynamodb.AttributeValue) bool {
	fmt.Printf(`HasEditAccess(), sk: "%v", item: %#v\n`, sk, item)

	if *item["sk"].S == sk {
		return true
	}

	if item["shrd"] == nil {
		return false
	}

	username := pure.Username(sk)
	if item["shrd"].M[username] == nil {
		return false
	}
	if *item["shrd"].M[username].S == "view" {
		return false
	}

	return true
}
