package mydynamo

import (
	"fmt"
	"testing"

	"../pure"

	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	email       = "vitaly@gms-ai.com"
	email2      = "maxim@gms-ai.com"
	projectName = "My project name, and this name is unique (I hope)"
	startDate   = "2020-04-05"
	dueDate     = "2021-12-31"
)

type params struct {
	Brand, Category, Segment, Channel, Currency string
	Models, Price                               int
	Countries, Specs, Competitors               []string `dynamodbav:",stringset"`
}

func init() {
	ProjectDelete(email, projectName)
}

func TestAddProject(t *testing.T) {
	// TODO
}

func TestProjectAttributeSet(t *testing.T) {
	p := params{
		Brand:    "My brand name",
		Category: "Washing",
		Segment:  "Front Loaders",
		Channel:  "all",
		Currency: "eur",

		Models: 5,
		Price:  90,

		Countries:   []string{"Belgium"},
		Specs:       []string{"RPM"},
		Competitors: []string{"Indesig"},
	}

	t.Run("owner", func(t *testing.T) {
		AddProject(email, projectName, startDate, dueDate)

		ProjectAttributeSet(email, pure.ExtractUsername(email), projectName, "params", p)

		item := GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("<<<<< Project from DynamoDB: %#v\n", item)

		checkSet(item, p, t)

		ProjectDelete(email, projectName)
	})
	t.Run("shareEdit", func(t *testing.T) {
		AddProject(email, projectName, startDate, dueDate)
		ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "edit", false)

		ProjectAttributeSet(email2, pure.ExtractUsername(email), projectName, "params", p)

		item := GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("<<<<< Project from DynamoDB: %#v\n", item)

		checkSet(item, p, t)

		ProjectDelete(email, projectName)
	})
	t.Run("shareView", func(t *testing.T) {
		AddProject(email, projectName, startDate, dueDate)
		ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "view", false)

		ProjectAttributeSet(email2, pure.ExtractUsername(email), projectName, "params", p)

		item := GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("<<<<< Project from DynamoDB: %#v\n", item)

		if _, ok := item["params"]; ok {
			t.Fatal()
		}

		ProjectDelete(email, projectName)
	})
	t.Run("notShared", func(t *testing.T) {
		AddProject(email, projectName, startDate, dueDate)

		ProjectAttributeSet(email2, pure.ExtractUsername(email), projectName, "params", p)

		item := GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("<<<<< Project from DynamoDB: %#v\n", item)

		if _, ok := item["params"]; ok {
			t.Fatal()
		}

		ProjectDelete(email, projectName)
	})
	t.Run("itemNotExists", func(t *testing.T) {
		ProjectAttributeSet(email, pure.ExtractUsername(email), projectName, "params", p)

		item := GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("<<<<< Project from DynamoDB: %#v\n", item)

		if _, ok := item["params"]; ok {
			t.Fatal()
		}
	})

}

func checkSet(item map[string]*dynamodb.AttributeValue, p params, t *testing.T) {
	if *item["params"].M["Brand"].S != p.Brand {
		t.Fatal("Brand is wrong :(")
	}
	if *item["params"].M["Category"].S != p.Category {
		t.Fatal("Category is wrong :(")
	}
	if *item["params"].M["Segment"].S != p.Segment {
		t.Fatal("Segment is wrong :(")
	}
	if *item["params"].M["Channel"].S != p.Channel {
		t.Fatal("Channel is wrong :(")
	}
	if *item["params"].M["Currency"].S != p.Currency {
		t.Fatal("Currency is wrong :(")
	}
	if *item["params"].M["Models"].N != fmt.Sprint(p.Models) {
		t.Fatal("Models is wrong :(")
	}
	if *item["params"].M["Price"].N != fmt.Sprint(p.Price) {
		t.Fatal("Price is wrong :(")
	}
	for i, country := range item["params"].M["Countries"].SS {
		if *country != p.Countries[i] {
			t.Fatal("Countries are wrong :(")
		}
	}
	for i, spec := range item["params"].M["Specs"].SS {
		if *spec != p.Specs[i] {
			t.Fatalf("Specs are wrong :( expected: %v, actual: %v", p.Specs[i], *spec)
		}
	}
	for i, spec := range item["params"].M["Competitors"].SS {
		if *spec != p.Competitors[i] {
			t.Fatal("Competitors are wrong :(")
		}
	}

	if *item["startDate"].S != startDate {
		t.Fatal("startDate is wrong :(")
	}
	if *item["dueDate"].S != dueDate {
		t.Fatal("dueDate is wrong :(")
	}

	if *item["entityStatus"].S != "In Progress" {
		t.Fatal()
	}
}

func TestProjectShare(t *testing.T) {
	usernameWithDot := "taras.bulba"
	shareWithDot := func(t *testing.T) {
		ProjectShare(pure.BuildSortKey(email, projectName), usernameWithDot, "view", false)

		item := GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("%#v\n", item)
		if item["shrd"] == nil {
			t.Fatal()
		}
		if *item["shrd"].M[usernameWithDot].S != "view" {
			t.Fatal()
		}
	}

	t.Run("SharedMapNew", func(t *testing.T) {
		AddProject(email, projectName, startDate, dueDate)

		ProjectShare(pure.BuildSortKey(email, projectName), "taras", "view", false)

		item := GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("%#v\n", item)
		if item["shrd"] == nil {
			t.Fatal()
		}
		if *item["shrd"].M["taras"].S != "view" {
			t.Fatal()
		}

		shareWithDot(t)

		ProjectDelete(email, projectName)
	})
	t.Run("SharedMapExisting", func(t *testing.T) {
		AddProject(email, projectName, startDate, dueDate)

		ProjectShare(pure.BuildSortKey(email, projectName), "taras", "view", false)
		ProjectShare(pure.BuildSortKey(email, projectName), "anton", "edit", true)

		item := GetProject(pure.BuildSortKey(email, projectName))
		if item["shrd"] == nil {
			t.Fatal()
		}
		if len(item["shrd"].M) != 2 {
			t.Fatal()
		}
		if *item["shrd"].M["taras"].S != "view" {
			t.Fatal()
		}
		if *item["shrd"].M["anton"].S != "edit" {
			t.Fatal()
		}

		shareWithDot(t)

		ProjectDelete(email, projectName)
	})
}

func TestProjectUnshare(t *testing.T) {
	AddProject(email, projectName, startDate, dueDate)
	ProjectShare(pure.BuildSortKey(email, projectName), "anton", "edit", false)

	ProjectUnshare(pure.BuildSortKey(email, projectName), "anton")

	item := GetProject(pure.BuildSortKey(email, projectName))
	fmt.Printf("Item: %#v\n", item)
	if len(item["shrd"].M) > 0 {
		t.Fatal()
	}

	ProjectShare(pure.BuildSortKey(email, projectName), "anton", "edit", false)
	ProjectShare(pure.BuildSortKey(email, projectName), "a.b", "edit", false)

	ProjectUnshare(pure.BuildSortKey(email, projectName), "anton")

	item = GetProject(pure.BuildSortKey(email, projectName))
	fmt.Printf("Item: %#v\n", item)
	if len(item["shrd"].M) > 1 {
		t.Fatal()
	}
	if *item["shrd"].M["a.b"].S != "edit" {
		t.Fatal()
	}

	ProjectDelete(email, projectName)
}
