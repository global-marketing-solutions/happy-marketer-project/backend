package myses

import (
	"testing"
)

func init() {
	InitWithoutSession()
}

func TestSend(t *testing.T) {
	Send(
		"vitaly@gms-ai.com",
		"Email from go test",
		"Hi my dear user, this is my demo body. I hope you like it :)",
	)
}
