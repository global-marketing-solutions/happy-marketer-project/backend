package myses

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
)

var (
	sesClient *ses.SES

	sendEmailInput = &ses.SendEmailInput{
		Source: aws.String("marketegy@gms-ai.com"),
	}
	message = &ses.Message{
		Subject: &ses.Content{
			Charset: aws.String("UTF-8"),
		},
		Body: &ses.Body{
			Text: &ses.Content{
				Charset: aws.String("UTF-8"),
			},
		},
	}
)

func InitWithSession(sess *session.Session) {
	sesClient = ses.New(sess)
}

func InitWithoutSession() {
	sess := session.Must(session.NewSession())
	sesClient = ses.New(sess)
}

func Send(email, subject, body string) {
	sendEmailInput.Destination = &ses.Destination{
		ToAddresses: []*string{
			&email,
		},
	}

	message.Subject.Data = &subject

	message.Body.Text.Data = &body

	sendEmailInput.SetMessage(message)

	fmt.Printf("sendEmailInput: %#v\n", sendEmailInput)

	// https://docs.aws.amazon.com/sdk-for-go/api/service/ses/#SES.SendEmail
	// https://docs.aws.amazon.com/code-samples/latest/catalog/go-ses-ses_send_email.go.html
	_, err := sesClient.SendEmail(sendEmailInput)
	if err != nil {
		fmt.Printf("ERROR sending email: %v\n", err)
	}
}
