package isAvailable

import (
	"../pure"

	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func IsAvailable(email, sk, statusRequested, statusActual string, shrd *dynamodb.AttributeValue) bool {
	return IsAccessMatch(email, sk, shrd) && isStatusMatch(statusRequested, statusActual)
}

func IsAccessMatch(email, sk string, shrd *dynamodb.AttributeValue) bool {
	if email == pure.Email(sk) {
		return true
	}
	if shrd == nil {
		return false
	}
	if _, ok := shrd.M[pure.ExtractUsername(email)]; ok {
		return true
	}
	return false
}

func isStatusMatch(statusRequested, statusActual string) bool {

	if statusActual == "" {
		return true
	}

	if statusRequested == "" {
		if statusActual == "In Progress" || statusActual == "Completed" {
			return true
		}
	}
	if statusRequested == statusActual {
		return true
	}
	return false
}
