package main

import (
	"context"
	"encoding/json"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

const (
	tableName = "marketegy"
)

var (
	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

type parameters struct {
	Brand, Category, Segment, Channel, Currency string
	Models, Price, MsrpVsMgp                    int
	PriceIndexRange                             *struct {
		Value   [2]uint16
		Percent float64
	}
	Specs                  []string
	Countries, Competitors []string `dynamodbav:",stringset"`
	Restrictions           *[]map[string]interface{}

	SelectedStateIds *struct {
		Step2 []string
		Step3 map[string][]string
	}
}

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	projectName := pure.DecodePercent(r.PathParameters["name"])

	var params parameters
	ownerUsername := r.QueryStringParameters["ownerUsername"]
	err := json.Unmarshal([]byte(r.Body), &params)
	if err != nil {
		panic(err)
	}

	mydynamo.ProjectAttributeSet(email, ownerUsername, projectName, "params", params)

	return resp, nil
}
