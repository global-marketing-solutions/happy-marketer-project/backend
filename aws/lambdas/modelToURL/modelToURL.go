package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var (
	client = &http.Client{}
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	l, imgSrc, err := scrapURLFromSERP(
		r.PathParameters["model"],
		r.QueryStringParameters["brand"],
		r.QueryStringParameters["isImage"] == "true",
	)

	if err != nil {

		return events.APIGatewayProxyResponse{
			StatusCode: 501,
		}, nil

	}

	if r.QueryStringParameters["isImage"] == "true" {
		b64, err := getImage(imgSrc)
		if err != nil {

			fmt.Println("ERROR on image getting:", err)

			return events.APIGatewayProxyResponse{
				StatusCode: 501,
			}, nil

		}

		return events.APIGatewayProxyResponse{
			StatusCode: 200,
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
				"Content-Type":                "image/webp",
				"Cache-Control":               "public, max-age=604800, immutable",
			},
			Body:            b64,
			IsBase64Encoded: true,
		}, nil

	}

	return events.APIGatewayProxyResponse{
		StatusCode: 308,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
			"Location":                    l,
		},
	}, nil

}

func scrapURLFromSERP(model, brand string, isImage bool) (string, string, error) {
	fmt.Println()

	var url string
	useragent := "Lynx/2.8.7rel.1 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/1.0.2a"

	protocol := "https://"
	var host string

	var re *regexp.Regexp
	var reImage *regexp.Regexp
	var reImageProductPage *regexp.Regexp

	var urlImagePrefix string

	switch brand {
	case "indesit":
		url = "https://www.indesit.it/id-search?q=" + model
		re = regexp.MustCompile(`(?s)product-list-item__code.+?a href="(.+?)"`)
		reImage = regexp.MustCompile(`(?s)product-list-item__image".+?img src="(.+?)"`)
	case "electrolux":
		url = "https://www.electrolux.it/search/?q=" + model
		re = regexp.MustCompile(`(?s)search-result-item.+?a href="(.+?)"`)
		reImage = regexp.MustCompile(`data-src="(.+?)&amp;width=`)
	case "lg":
		url = "https://www.lg.com/it/search/search-all?search=" + model
		re = regexp.MustCompile(`(?s)schema.org/Product.+?a href="(.+?)"`)
		reImage = regexp.MustCompile(`(?s)schema.org/Product.+?img data-src="(.+?)"`)
	case "beko":
		url = "https://www.beko.com/it-it/Risultati-della-ricerca?search=" + strings.ToUpper(model)
		re = regexp.MustCompile(`(?s)ProductCardSearch__model.+?href="(.+?)"`)
		reImage = regexp.MustCompile(`(?s)ProductCardSearch__model.+?src="(.+?)"`)
	case "samsung":
		url = "https://esapi.samsung.com/search/query/v6?start=0&num=1&categoryTab=all&siteCd=it&searchValue=" + model
		re = regexp.MustCompile(`"linkUrl":"(.+?)"`)
		reImage = regexp.MustCompile(`"thumbnailUrl":"(.+?)"`)
		host = "samsung.com"
		urlImagePrefix = "https://images.samsung.com/is/image/samsung/"
	case "whirlpool":
		url = "https://www.whirlpool.it/wp-search-auto-suggestion?q=" + model
		re = regexp.MustCompile(`"x_productURL":"(.+?)"`)
		reImage = regexp.MustCompile(`"fullImage":"(.+?)\?`)
	case "hotpoint-aris":
		url = "https://www.hotpoint.it/hp-search/autocomplete?q=" + model
		re = regexp.MustCompile(`"href":"(.+?)"`)
		reImage = regexp.MustCompile(`"image":"(.+?)\?`)
	case "candy":
		url = "https://www.candy.it/it_IT/ricerca?p_p_id=3&_3_struts_action=/search/search&_3_keywords=" + model
		re = regexp.MustCompile(`search-results_content.+?href="(.+?)"`)
		reImageProductPage = regexp.MustCompile(`class="slide" .+? src="(.+?)"`)
	case "hoover":
		url = "https://www.hoover.it/it_IT/ricerca?p_p_id=3&_3_struts_action=/search/search&_3_keywords=" + model
		re = regexp.MustCompile(`search-results__content.+?href="(.+?)"`)
		reImageProductPage = regexp.MustCompile(`img src="(.+?)" .+? itemprop="image"`)
	case "bosch":
		url = "https://www.bosch-home.com/it/ajax/supportlist?searchTerm=" + model
		re = regexp.MustCompile(`"href":"(.+?)"`)
		useragent = "Mozilla/5.0 (X11; Linux i686; rv:84.0) Gecko/20100101 Firefox/84.0"
		reImage = regexp.MustCompile(`"src":"(.+?)"`)
	case "siemens":
		url = "https://www.siemens-home.bsh-group.com/it/ajax/supportlist?searchTerm=" + model
		re = regexp.MustCompile(`"href":"(.+?)"`)
		useragent = "Mozilla/5.0 (X11; Linux i686; rv:84.0) Gecko/20100101 Firefox/84.0"
		reImage = regexp.MustCompile(`"src":"(.+?)"`)
	case "smeg":
		url = "https://www.smeg.com/search?query=" + model
		re = regexp.MustCompile(`href="(/products/.+?)"`)
		reImage = regexp.MustCompile(`(https://assets.4flow.cloud/.+?)"`)
	case "comfee":
		url = "https://www.comfee.eu/?s=" + model
		re = regexp.MustCompile(`"entry-title".+?href="(.+?)"`)
		reImage = regexp.MustCompile(`(?s)id="searchloop".+? src="(.+?)"`)
	case "aeg":
		url = "https://www.aeg.it/search/?q=" + model
		re = regexp.MustCompile(`(?s)search-result-item.+?href="(.+?)"`)
		reImage = regexp.MustCompile(`data-src="(.+?)&amp;width=`)
	case "ignis":
		url = "http://www.ignis.it/search/search.cfm?TextWebSite=" + model
		re = regexp.MustCompile(`(?s)item-list.+?href="(.+?)"`)
		protocol = "http://"
		reImageProductPage = regexp.MustCompile(`src="(../immagini/prodotti/prodZoom/.+?jpg)"`)
	}

	fmt.Println("url:", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", "", err
	}

	req.Header.Set("User-Agent", useragent)
	req.Header.Set("Accept-Language", "en-US")
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	fmt.Printf("before Do, req: %#v\n", req)
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	fmt.Println("resp.Status: ", resp.Status)

	bts, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	submatch := re.FindSubmatch(bts)

	if len(submatch) == 0 {
		f, err := os.Create("/tmp/serp.html")
		if err != nil {
			fmt.Println("Cannot create file:", err)
		}
		defer f.Close()
		_, err = f.WriteString(string(bts))
		if err != nil {
			fmt.Println("ERROR writing file:", err)
		}

		return "", "", fmt.Errorf("Unable to extract URL from HTML, response: %#v\n", resp)
	}
	fmt.Println("submatch:", string(submatch[0]))

	if host == "" {
		host = resp.Request.URL.Host
	}

	if isImage {
		if reImage == nil {
			urlProductPage := string(submatch[1])
			if strings.HasPrefix(urlProductPage, "..") {
				urlProductPage = protocol + host + strings.Replace(urlProductPage, "..", "", 1)
			}
			bts, err := getProductPage(urlProductPage)
			fmt.Println("html of product page:", string(bts))
			if err != nil {
				return "", "", err
			}
			submatchProductPage := reImageProductPage.FindSubmatch(bts)
			if strings.HasPrefix(string(submatchProductPage[1]), "..") {
				return "",
					protocol + host + strings.Replace(string(submatchProductPage[1]), "..", "", 1),
					nil
			}
			return "", string(submatchProductPage[1]), nil
		}

		submatchSrc := reImage.FindSubmatch(bts)
		fmt.Println("submatchSrc:", string(submatchSrc[0]))
		if len(submatchSrc) == 0 {
			return "", "", fmt.Errorf("No image found")
		}

		urlImage := strings.Replace(string(submatchSrc[1]), "&amp;", "&", 1)
		if strings.HasPrefix(urlImage, "//") {
			return "", "https:" + strings.Replace(urlImage, "{width}x{height}", "600x600", 1), nil
		}
		if strings.HasPrefix(urlImage, "/") {
			return "", protocol + host + urlImage, nil
		}
		return "", urlImagePrefix + strings.ReplaceAll(urlImage, `\/`, "/"), nil
	}

	if strings.HasPrefix(string(submatch[1]), "https:") {
		return strings.ReplaceAll(string(submatch[1]), `\/`, "/"), "", nil
	}

	return protocol + host + strings.ReplaceAll(strings.ReplaceAll(string(submatch[1]), `\/`, "/"), "..", ""),
		"",
		nil
}

func getImage(imgSrc string) (string, error) {
	req, err := http.NewRequest("GET", imgSrc, nil)
	if err != nil {
		return "", err
	}
	req.Header.Set("User-Agent", "Lynx/2.8.7rel.1 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/1.0.2a")
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	bts, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	return base64.StdEncoding.EncodeToString(bts), nil
}

func getProductPage(url string) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return []byte{}, err
	}
	req.Header.Set("User-Agent", "Lynx/2.8.7rel.1 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/1.0.2a")
	resp, err := client.Do(req)
	if err != nil {
		return []byte{}, err
	}

	bts, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, err
	}

	return bts, nil
}
