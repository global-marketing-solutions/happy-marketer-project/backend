package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestScrapURLFromSERP(t *testing.T) {
	url, _, err := scrapURLFromSERP(strings.ReplaceAll("iwb 51251 c eco eu", " ", "%20"), "indesit", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("EW6S526W", "electrolux", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("f4j6j10kg", "lg", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("wtx51021w", "beko", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("WW9NK4430YW", "samsung", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("best%20zen%208", "whirlpool", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("eu%20aq%20497d%2079d", "hotpoint-aris", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("CSS%20128T3-01", "candy", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("DXOC34%2026C3/2", "hoover", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("WAJ20007IT", "bosch", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("wm12t457it", "siemens", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("wht812lsit", "smeg", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("mfe812", "comfee", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("l7fee842", "aeg", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, _, err = scrapURLFromSERP("ig%20g91284%20it", "ignis", false)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}

	url, imgSrc, err := scrapURLFromSERP("iwb%2051251%20c%20eco%20eu", "indesit", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://www.whirlpool.eu/digitalassets/Picture/web320x320/F087169_320x320_view1.png" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("EW6S526W", "electrolux", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc !=
		"https://www.electrolux.it/remote.jpg.ashx?urlb64=aHR0cHM6Ly9zZXJ2aWNlcy5lbGVjdHJvbHV4LW1lZGlhbGlicmFyeS5jb20vMTE4ZWQ0YzBlZTY1NDZmNGE3Njg0YzdmZWY4Yzk4NWFOclptWWtNODYxZDFmL3ZpZXcvV1NfUE4vUFNFRVdNMjAwUEEwMDAzTC5wbmc&hmac=9V5-tDDe9mg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("f4j6j10kg", "lg", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://www.lg.com/it/images/lavatrici/md06041897/lg-lavatrice-F4J6J10KG-thumb_350_V_11-02-2019.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("wtx51021w", "beko", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://www.beko.com/content/dam/italy-it-aem/italy-it-aemProductCatalog/product-images/7139241800-WTX51021W/7139241800-LO1-20170524-111251.png.transform/rendition-503/image.png" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("WW9NK4430YW", "samsung", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://images.samsung.com/is/image/samsung/it-washer-ww9nk4430yw-ww9nk4430yw-et-frontwhite-thumb-162362112" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("best%20zen%208", "whirlpool", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://whirlpool-cdn.thron.com/delivery/public/thumbnail/whirlpool/pi-232e7e16-8702-439c-ad21-cf4eb67ae823/jsind9/std/320x320/BEST-ZEN-8-Lavatrici.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("eu%20aq%20497d%2079d", "hotpoint-aris", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://whirlpool-cdn.thron.com/delivery/public/thumbnail/whirlpool/pi-967e5c3c-7e3f-4140-831f-a650c165cdf9/jsind9/std/320x320/eu-aq-497d-79d-lavatrici.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("CSS%20128T3-01", "candy", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://d15v10x8t3bz3x.cloudfront.net/Immagini/2018/9/15374445/10-31008203_CSS%20128T3-01-F-60188-M.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("DXOC34%2026C3/2", "hoover", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://d15v10x8t3bz3x.cloudfront.net/Immagini/2018/9/15374423/10-31007895_DXOC34%2026C32-01-F-58690-M.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("WAJ20007IT", "bosch", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://media3.bosch-home.com/Product_Shots/600x600/MCSA02990583_WAJ20007IT_STP_def.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("wm12t457it", "siemens", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://media3.bsh-group.com/Product_Shots/600x600/MCSA01857616_WM12T457IT_def.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("wht812lsit", "smeg", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://assets.4flow.cloud/WHT812LSIT.jpg?pEFs=cVY2M1MyN1ZOMFFadEQ5ZlVOMzhVNTYvK2RrNll5N2l0TUwvLzNWR2ppTT0" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("mfe812", "comfee", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "https://www.comfee.eu/wp-content/uploads/Immagine-Comfee-MFE-1200x908.jpg" {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("l7fee842", "aeg", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if !strings.HasPrefix(imgSrc, "https://www.aeg.it/remote.jpg.ashx?urlb64=aHR0cHM6Ly9zZXJ2aWNlcy5lbGVjdHJvbHV4LW1lZGlhbGlicmFyeS5jb20vMTE4ZWQ0YzBlZTY1NDZmNGE3Njg0YzdmZWY4Yzk4NWFOclptWWtNODYxZDFmL3ZpZXcvV1NfUE4vUFNBQVdNMjAwUEEwMDA0WC5wbmc&hmac=") {
		panic("Wrong imgSrc")
	}

	url, imgSrc, err = scrapURLFromSERP("ig%20g91284%20it", "ignis", true)
	fmt.Println(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("imgSrc:", imgSrc)
	if imgSrc != "http://www.ignis.it/immagini/prodotti/prodZoom/869991576040.jpg" {
		panic("Wrong imgSrc")
	}

}

func TestGetImage(t *testing.T) {
	fmt.Println(
		getImage("https://images.samsung.com/is/image/samsung/it-washer-ww70j5255mw-ww70j5255mw-et-white-thumb-White-150698800"),
	)
}
