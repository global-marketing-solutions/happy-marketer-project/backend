package main

import (
	"context"
	"encoding/json"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	dynamodbClient = dynamodb.New(mydynamo.Sess)
)

type userModel struct {
	Id    string
	Price uint16
	Specs map[string]string
}

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	documentName := pure.DecodePercent(r.PathParameters["name"])
	ownerUsername := r.QueryStringParameters["ownerUsername"]
	userModels := sliceOfModelsUnmarshal(r.Body)

	modelsPut(email, ownerUsername, documentName, mydynamo.Marshal(userModels))

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}, nil
}

func sliceOfModelsUnmarshal(s string) (userModels []userModel) {
	err := json.Unmarshal([]byte(s), &userModels)
	if err != nil {
		panic(err)
	}
	return
}

func modelsPut(email, ownerUsername, documentName string, userModels *dynamodb.AttributeValue) {
	updateItemInput := dynamodb.UpdateItemInput{
		TableName: aws.String("marketegy"),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("document"),
			},
			"sk": {
				S: aws.String(pure.ExtractDomain(email) + " " + ownerUsername + " " + documentName),
			},
		},
		ExpressionAttributeNames: map[string]*string{
			"#username": aws.String(pure.ExtractUsername(email)),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":sk": {
				S: aws.String(pure.BuildSortKey(email, documentName)),
			},
			":mode": {
				S: aws.String("edit"),
			},
		},
		ConditionExpression: aws.String(mydynamo.ConditionExpression()),
	}

	if userModels == nil {
		updateItemInput.UpdateExpression = aws.String("REMOVE models")
	} else {
		updateItemInput.UpdateExpression = aws.String("SET models = :models")
		updateItemInput.ExpressionAttributeValues[":models"] = userModels
	}

	_, err := dynamodbClient.UpdateItem(&updateItemInput)
	if err != nil {
		mydynamo.ErrCheck(err)
	}
}
