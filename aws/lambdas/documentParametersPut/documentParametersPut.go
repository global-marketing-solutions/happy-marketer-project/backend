package main

import (
	"context"
	"encoding/json"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	tableName = "marketegy"
)

var (
	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

type params struct {
	Brand, Currency, TypeOfPrice string
	Specs                        []struct {
		Name   string
		Values []string
	}
	SpecsPercentsX []uint
	SpecsPercentsY []uint
}

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	projectName := pure.DecodePercent(r.PathParameters["name"])

	var prms params
	ownerUsername := r.QueryStringParameters["ownerUsername"]
	err := json.Unmarshal([]byte(r.Body), &prms)
	if err != nil {
		panic(err)
	}

	documentParametersPut(email, ownerUsername, projectName, prms)

	return resp, nil
}

func documentParametersPut(email, ownerUsername, projectName string, prms params) {
	sk := pure.BuildSortKeyForOwner(email, ownerUsername, projectName)
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName: aws.String("marketegy"),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("document"),
			},
			"sk": {
				S: &sk,
			},
		},
		UpdateExpression: aws.String("SET params = :params"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":sk": {
				S: aws.String(pure.BuildSortKey(email, projectName)),
			},
			":mode": {
				S: aws.String("edit"),
			},
			":params": mydynamo.Marshal(prms),
		},
		ConditionExpression: aws.String(mydynamo.ConditionExpression()),
		ExpressionAttributeNames: map[string]*string{
			"#username": aws.String(pure.ExtractUsername(email)),
		},
	}

	mydynamo.UpdateItem(updateItemInput)
}
