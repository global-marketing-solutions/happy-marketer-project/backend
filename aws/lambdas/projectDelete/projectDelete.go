package main

import (
	"context"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	projectName := pure.DecodePercent(r.PathParameters["name"])
	ownerUsername := r.QueryStringParameters["ownerUsername"]

	if ownerUsername == "" {
		mydynamo.ProjectDelete(email, projectName)
	} else {
		sk := pure.BuildSortKey(ownerUsername+"@"+pure.ExtractDomain(email), projectName)
		mydynamo.ProjectUnshare(sk, pure.ExtractUsername(email))
	}

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}, nil
}
