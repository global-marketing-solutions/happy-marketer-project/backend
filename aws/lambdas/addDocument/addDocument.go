package main

import (
	"context"
	"encoding/json"

	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	Sess           = session.Must(session.NewSession())
	DynamodbClient = dynamodb.New(Sess)

	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

type event struct {
	Name      string
	StartDate string
	DueDate   string
}

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)

	var e event
	err := json.Unmarshal([]byte(r.Body), &e)
	if err != nil {
		panic(err)
	}

	putItemInput := dynamodb.PutItemInput{
		TableName: aws.String("marketegy"),
		Item: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("document"),
			},
			"sk": {
				S: aws.String(pure.BuildSortKey(email, e.Name)),
			},
			"startDate": {
				S: &e.StartDate,
			},
			"dueDate": {
				S: &e.DueDate,
			},
			"entityStatus": {
				S: aws.String("In Progress"),
			},
		},
		ConditionExpression: aws.String("attribute_not_exists(sk)"),
	}
	_, err = DynamodbClient.PutItem(&putItemInput)
	if err != nil {
		panic(err)
	}

	return resp, nil
}
