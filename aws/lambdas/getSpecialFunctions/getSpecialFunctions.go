package main

import (
	"context"
	"encoding/json"
	"fmt"

	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	cache string
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)

	var body string
	if len(cache) == 0 {
		b, err := json.Marshal(getCategoriesWithBrands(pure.Subdomain(email)))
		if err != nil {
			panic(err)
		}
		body = string(b)
	} else {
		body = cache
	}

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
		Body: body,
	}, nil
}

func getCategoriesWithBrands(domain string) map[string]map[string][]string {
	sess := session.Must(session.NewSession())
	dynamoClient := dynamodb.New(sess)

	getItemInput := &dynamodb.GetItemInput{
		TableName: aws.String("marketegy"),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("settings"),
			},
			"sk": {
				S: &domain,
			},
		},
	}

	queryOutput, err := dynamoClient.GetItem(getItemInput)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%#v\n", queryOutput)

	out := make(map[string]map[string][]string)
	for category, brandsToFuncs := range queryOutput.Item {
		if brandsToFuncs.M == nil {
			continue
		}

		if out[category] == nil {
			out[category] = map[string][]string{}
		}

		for brand, sfs := range brandsToFuncs.M {
			for _, sf := range sfs.SS {
				out[category][brand] = append(out[category][brand], *sf)
			}
		}

	}

	return out
}
