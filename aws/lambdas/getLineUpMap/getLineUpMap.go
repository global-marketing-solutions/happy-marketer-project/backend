package main

import (
	"fmt"
	"sort"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	sess           = session.Must(session.NewSession())
	dynamodbClient = dynamodb.New(sess)
)

type event struct {
	Email string

	Countries, Specs           []string
	Category, Segment, Channel string

	NumberOfModels int

	Restrictions []mydynamo.Rstrs

	PriceRange [2]uint16
}

func main() {
	lambda.Start(handleRequest)
}

func handleRequest(event event) (out, error) {
	fmt.Printf("event: %#v\n", event)

	o := getReports(event)
	o.calcPercentsForView()

	sort.Slice(o.Models, func(i, j int) bool {
		return o.Models[i].value > o.Models[j].value
	})

	fmt.Println("amount of models after merging:", len(o.Models))
	for i, m := range o.Models {
		fmt.Printf("%#v\n", m)
		if i > 10 {
			break
		}
	}

	if len(o.Models) > event.NumberOfModels && event.NumberOfModels != 0 {
		o.Models = o.Models[:event.NumberOfModels]
	}

	return o, nil
}

func getReports(e event) (o out) {

	o.SpecsAllPossible = make(map[string][]string)

	err := dynamodbClient.QueryPages(
		mydynamo.RprtsQuery(
			e.Countries,
			e.Category,
			e.Segment,
			e.Channel,
			pure.Subdomain(e.Email),
			e.PriceRange,
			e.Restrictions),
		func(page *dynamodb.QueryOutput, lastPage bool) bool {
			fmt.Println("Reports from DynamoDB: page size:", len(page.Items))
			for _, item := range page.Items {
				o.addValue(*item["value"].N)
				o.add(item, e.Specs)
			}
			return true
		},
	)
	if err != nil {
		panic(err)
	}

	return
}

type out struct {
	Segments         xys
	Models           []model
	SpecsAllPossible specsAllPossible

	value float64
}

type model struct {
	Specs map[string]string
	value float64
}

func (m model) findByAllSpecs(models []model) (int, bool) {
outer:
	for i, _m := range models {
		for name, val := range _m.Specs {
			v, ok := m.Specs[name]
			if ok && v != val {
				continue outer
			}
		}
		return i, true
	}
	return -1, false
}

type specsAllPossible map[string][]string

func (s specsAllPossible) absorb(m model) {
	for specName, specValue := range m.Specs {
		for _, _specValue := range s[specName] {
			if specValue == _specValue {
				return
			}
		}
		s[specName] = append(s[specName], specValue)
	}
}

type xys []xy
type xy struct {
	value float64

	X       string
	Y       string
	Brand   string
	Percent int
}

func (s xys) columns() (columns []string) {
outer:
	for _, xy := range s {
		for _, column := range columns {
			if xy.Y == column {
				continue outer
			}
		}
		columns = append(columns, xy.Y)
	}
	return
}

func (o *out) addValue(value string) float64 {
	f := pure.StrToFloat64(value)
	o.value += f
	return f
}
func (o *out) add(item map[string]*dynamodb.AttributeValue, specs []string) {
	m := model{
		Specs: mydynamo.ItemToSpecs(item, specs),
		value: pure.StrToFloat64(*item["value"].N),
	}
	xy := xy{
		X: m.Specs[specs[0]],
		Y: m.Specs[specs[1]],

		value: m.value,
	}

	if i, ok := o.findByXYSpecs(xy); ok {
		o.Segments[i].value += m.value
	} else {
		o.Segments = append(o.Segments, xy)
	}

	if i, ok := m.findByAllSpecs(o.Models); ok {
		o.Models[i].value += m.value
	} else {
		o.Models = append(o.Models, m)
	}

	o.SpecsAllPossible.absorb(m)
}
func (o *out) findByXYSpecs(xy xy) (int, bool) {
	for i, _xy := range o.Segments {
		if _xy.X == xy.X && _xy.Y == xy.Y {
			return i, true
		}
	}
	return -1, false
}

func includes(slice []string, str string) bool {
	for _, _str := range slice {
		if str == _str {
			return true
		}
	}
	return false
}

func (o *out) calcPercentsForView() {
	onePercent := o.value / 100
	for i, xy := range o.Segments {
		o.Segments[i].Percent = int(xy.value / onePercent)
	}
}
