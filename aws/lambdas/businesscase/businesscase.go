package main

import (
	"context"
	"encoding/json"
	"fmt"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

const (
	tableName = "marketegy"
)

var (
	dynamodbClient = dynamodb.New(mydynamo.Sess)

	updateItemInput = dynamodb.UpdateItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":status": {
				S: aws.String("In Progress"),
			},
		},
	}

	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("request: %#v\n", r)
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	projectName := pure.DecodePercent(r.PathParameters["name"])
	owner := r.QueryStringParameters["owner"]

	businesscasePut(email, owner, projectName, r.Body)

	return resp, nil
}

func businesscasePut(email, owner, projectName, businesscase string) {
	fmt.Printf("businesscasePut(): updateItemInput at the start of function: %#v\n", updateItemInput)

	sk := pure.BuildSortKeyForOwner(email, owner, projectName)
	updateItemInput.Key["sk"] = &dynamodb.AttributeValue{
		S: &sk,
	}

	updateItemInput.UpdateExpression = aws.String("SET businesscase = :businesscase, entityStatus = :status")
	updateItemInput.ExpressionAttributeValues = map[string]*dynamodb.AttributeValue{
		":businesscase": {
			M: marshal(businesscase).M,
		},
		":status": {
			S: aws.String("In Progress"),
		},
		":sk": {
			S: aws.String(pure.BuildSortKey(email, projectName)),
		},
		":mode": {
			S: aws.String("edit"),
		},
	}
	updateItemInput.ConditionExpression = aws.String(mydynamo.ConditionExpression())
	updateItemInput.ExpressionAttributeNames = map[string]*string{
		"#username": aws.String(pure.ExtractUsername(email)),
	}

	fmt.Printf("businesscasePut(): updateItemInput before using: %#v\n", updateItemInput)
	_, err := dynamodbClient.UpdateItem(&updateItemInput)
	if err != nil {
		mydynamo.ErrCheck(err)
	}
}

func marshal(data string) *dynamodb.AttributeValue {
	var sl map[string]interface{}
	err := json.Unmarshal([]byte(data), &sl)
	if err != nil {
		panic(err)
	}
	attr, err := dynamodbattribute.Marshal(sl)
	if err != nil {
		panic(err)
	}

	fmt.Printf("marshal() resp: %#v\n", attr)

	return attr
}
