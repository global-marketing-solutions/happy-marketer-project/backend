package main

import (
	"fmt"
	"testing"
)

func TestMarshal(t *testing.T) {
	t.Run("first", func(t *testing.T) {
		v := marshal(`{
			"specs":["display","rpm"],
			"financials":["msrp","discounts_p"],
			"kpi":"volume",
			"period":"by_quarters"
		}`)
		fmt.Printf("value: %#v\n", v)

		if *v.M["kpi"].S != "volume" {
			t.Fail()
		}
		if len(v.M["specs"].L) != 2 {
			t.Fail()
		}

	})

	t.Run("second", func(t *testing.T) {

	})
}
