package main

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	tableName = "marketegy"
)

var (
	dynamodbClient = dynamodb.New(mydynamo.Sess)

	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

type userModel struct {
	Price, Msrp int
	Specs       map[string]string
	Fin, Sums   map[string]int
}

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	projectName := pure.DecodePercent(r.PathParameters["name"])
	ownerUsername := r.QueryStringParameters["ownerUsername"]
	if r.Body == "[]" {
		common(email, ownerUsername, projectName, nil)
	} else {
		userModels := sliceOfModelsUnmarshal(r.Body)
		spNms := specNames(userModels[0])
		go common(email, ownerUsername, projectName, mydynamo.Marshal(userModels))

		cntrs := sliceUnmarshal(r.QueryStringParameters["countries"])
		ctgr := r.QueryStringParameters["category"]
		sgmnt := r.QueryStringParameters["segment"]
		chnl := r.QueryStringParameters["channel"]
		prcRng := priceRangeUnmarshal(r.QueryStringParameters["priceRange"])
		rstrs := restrictionsUnmarshal(r.QueryStringParameters["restrictions"])

		ch := make(chan out)
		go func() {
			rps := getModelsByParams(cntrs, spNms, ctgr, sgmnt, chnl, pure.Subdomain(email), prcRng, rstrs)
			calc(rps, userModels, ch)
		}()
		b, err := json.Marshal(<-ch)
		if err != nil {
			panic(err)
		}
		resp.Body = string(b)

	}
	return resp, nil
}

func common(email, owner, project string, userModels *dynamodb.AttributeValue) {
	updateItemInput := dynamodb.UpdateItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
			"sk": {
				S: aws.String(pure.ExtractDomain(email) + " " + owner + " " + project),
			},
		},
		ExpressionAttributeNames: map[string]*string{
			"#username": aws.String(pure.ExtractUsername(email)),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":sk": {
				S: aws.String(pure.BuildSortKey(email, project)),
			},
			":status": {
				S: aws.String("In Progress"),
			},
			":mode": {
				S: aws.String("edit"),
			},
		},
		ConditionExpression: aws.String(mydynamo.ConditionExpression()),
	}

	if userModels == nil {
		updateItemInput.UpdateExpression = aws.String("REMOVE models SET entityStatus = :status")
	} else {
		updateItemInput.UpdateExpression = aws.String("SET models = :models, entityStatus = :status")
		updateItemInput.ExpressionAttributeValues[":models"] = userModels
	}

	fmt.Printf("common(): updateItemInput: %#v\n", updateItemInput)

	_, err := dynamodbClient.UpdateItem(&updateItemInput)
	if err != nil {
		mydynamo.ErrCheck(err)
	}
}

func marshal(data string) *dynamodb.AttributeValue {
	var sl []map[string]interface{}
	err := json.Unmarshal([]byte(data), &sl)
	if err != nil {
		panic(err)
	}

	return mydynamo.Marshal(sl)
}

func sliceOfModelsUnmarshal(s string) (userModels []userModel) {
	err := json.Unmarshal([]byte(s), &userModels)
	if err != nil {
		panic(err)
	}
	return
}

func specNames(userModel userModel) (spNms []string) {
	for spN := range userModel.Specs {
		spNms = append(spNms, spN)
	}
	return
}

func sliceUnmarshal(s string) (sl []string) {
	err := json.Unmarshal([]byte(s), &sl)
	if err != nil {
		panic(err)
	}
	return
}

func priceRangeUnmarshal(priceRange string) (prcRng [2]uint16) {
	if priceRange == "" {
		return [2]uint16{0, 0}
	}
	err := json.Unmarshal([]byte(priceRange), &prcRng)
	if err != nil {
		panic(err)
	}
	return
}

func restrictionsUnmarshal(restrictions string) (rstrs []mydynamo.Rstrs) {
	err := json.Unmarshal([]byte(restrictions), &rstrs)
	if err != nil {
		panic(err)
	}
	return
}

func getModelsByParams(
	cntrs, spNms []string,
	ctgr, sgmnt, chnl, dmn string,
	prcRng [2]uint16,
	rstrs []mydynamo.Rstrs) (models []mydynamo.Model) {

	err := mydynamo.DynamodbClient.QueryPages(
		mydynamo.RprtsQuery(
			cntrs,
			ctgr,
			sgmnt,
			chnl,
			dmn,
			prcRng,
			rstrs),
		func(page *dynamodb.QueryOutput, lastPage bool) bool {
			fmt.Println("Reports from DynamoDB: page size:", len(page.Items))
			for _, item := range page.Items {
				m, err := mydynamo.NewModel(item, spNms)
				if err != nil {
					panic(err)
				}
				if i, ok := mydynamo.FindByAllSpecs(models, m); ok {
					models[i].Value += m.Value
					continue
				}
				models = append(models, m)
			}
			return true
		},
	)
	if err != nil {
		panic(err)
	}

	return models
}

type out []mydynamo.Specs

func calc(models []mydynamo.Model, userModels []userModel, ch chan<- out) {
	var modelsMatched []mydynamo.Model

outer:
	for _, um := range userModels {
		for _, m := range models {
			if m.Specs.EqByAllSpecs(um.Specs) {
				modelsMatched = append(modelsMatched, m)
				continue outer
			}
		}
	}

	sort.Slice(modelsMatched, func(i, j int) bool {
		return modelsMatched[i].Value > modelsMatched[j].Value
	})

	var out out
	for _, m := range modelsMatched {
		out = append(out, m.Specs)
	}

	ch <- out
}
