package main

import (
	"fmt"
	"testing"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	email         = "vitaly@gms-ai.com"
	email2        = "xxx@gms-ai.com"
	ownerUsername = pure.ExtractUsername(email)
	projectName   = "My project name, and this name is unique (I hope)"
	startDate     = "2020-04-05"
	dueDate       = "2021-12-31"
	models        = `[
		{
			"Capacity": 7,
			"Energy": "A+"
		},
		{
			"Capacity": 7,
			"Energy": "A++"
		},
		{
			"Capacity": 7,
			"Energy": "A+++"
		},
		{
			"Capacity": 7,
			"Energy": "A++"
		},
		{
			"Capacity": 8,
			"Energy": "A+++"
		}
	]`
	models2 = `[
		{
			"Capacity": 9,
			"Energy": "B"
		},
		{
			"Capacity": 1,
			"Energy": "B"
		}
	]`

	modelsMock = []mydynamo.Model{
		mydynamo.Model{Value: 692544.7000000001, Units: 98, ID: "1593121372518342", Brand: "bosch", Model: "b 27", Distribution: "90", TOS: "30", Price: "319.2", Specs: mydynamo.Specs{"capacity": "7", "energy": "a++"}},
		mydynamo.Model{Value: 687691.76, Units: 88, ID: "1593121372518423", Brand: "bosch", Model: "b 45", Distribution: "89.6", TOS: "29.9", Price: "379.2", Specs: mydynamo.Specs{"capacity": "8", "energy": "a++"}},
		mydynamo.Model{Value: 563460.2600000002, Units: 80, ID: "1593121372518456", Brand: "bosch", Model: "b 29", Distribution: "88.8", TOS: "29.6", Price: "322.8", Specs: mydynamo.Specs{"capacity": "7", "energy": "a+++"}},
		mydynamo.Model{Value: 491893.4699999998, Units: 78, ID: "1593121372518612", Brand: "bosch", Model: "b 09", Distribution: "88.4", TOS: "29.5", Price: "259.2", Specs: mydynamo.Specs{"capacity": "6", "energy": "a++"}},
		mydynamo.Model{Value: 540046.76, Units: 75, ID: "1593121372518755", Brand: "bosch", Model: "b 25", Distribution: "87.8", TOS: "29.3", Price: "315.6", Specs: mydynamo.Specs{"capacity": "7", "energy": "a+"}},
		mydynamo.Model{Value: 531716.4, Units: 70, ID: "1593121372518826", Brand: "bosch", Model: "b 47", Distribution: "85.6", TOS: "28.5", Price: "382.8", Specs: mydynamo.Specs{"capacity": "8", "energy": "a+++"}},
		mydynamo.Model{Value: 508833.18999999994, Units: 65, ID: "1593121372518868", Brand: "bosch", Model: "b 43", Distribution: "82", TOS: "27.3", Price: "375.6", Specs: mydynamo.Specs{"capacity": "8", "energy": "a+"}},
		mydynamo.Model{Value: 387429.52999999997, Units: 60, ID: "1593121372518939", Brand: "bosch", Model: "b 11", Distribution: "76.4", TOS: "25.4", Price: "262.8", Specs: mydynamo.Specs{"capacity": "6", "energy": "a+++"}},
		mydynamo.Model{Value: 368027.95, Units: 55, ID: "1593121372519175", Brand: "bosch", Model: "b 07", Distribution: "68.8", TOS: "22.8", Price: "255.6", Specs: mydynamo.Specs{"capacity": "6", "energy": "a+"}},
	}
)

func init() {
	mydynamo.ProjectDelete(email, projectName)
	mydynamo.ProjectDelete(email2, projectName)
}

func TestModelsPut(t *testing.T) {
	t.Run("owner", func(t *testing.T) {

		mydynamo.AddProject(email, projectName, startDate, dueDate)

		common(email, ownerUsername, projectName, marshal(models))

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("TEST: item returned: %#v\n", item)

		check(item, email, t)

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("shareEdit", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "edit", false)

		common(email2, ownerUsername, projectName, marshal(models))

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		check(item, email, t)

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("shareView", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "view", false)

		common(email2, ownerUsername, projectName, marshal(models))

		item := mydynamo.GetProject(pure.BuildSortKey(email2, projectName))
		if _, ok := item["models"]; ok {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("notShared", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)

		common(email2, ownerUsername, projectName, marshal(models))

		item := mydynamo.GetProject(pure.BuildSortKey(email2, projectName))
		if _, ok := item["models"]; ok {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("itemNotExists", func(t *testing.T) {
		common(email2, "ownerUsernameThatNotExists", projectName, marshal(models))

		item := mydynamo.GetProject(pure.BuildSortKey(email2, projectName))
		if _, ok := item["models"]; ok {
			t.Fatal()
		}
	})
	t.Run("restorePreviousModels", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)

		common(email, ownerUsername, projectName, marshal(models))

		common(email2, ownerUsername, projectName, marshal(models2))

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("TEST: item returned: %#v\n", item)
		if len(item["models"].L) != 5 {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("projectStatusAltered", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.Complete(email, ownerUsername, projectName)

		common(email, ownerUsername, projectName, marshal(models))

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		check(item, email, t)

		mydynamo.ProjectDelete(email, projectName)
	})

}

func TestModelsRemove(t *testing.T) {
	t.Run("owner", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)

		common(email, ownerUsername, projectName, marshal(models))

		common(email, ownerUsername, projectName, nil)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		if _, ok := item["models"]; ok {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("shareEdit", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "edit", false)

		common(email, ownerUsername, projectName, marshal(models))

		common(email2, ownerUsername, projectName, nil)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		if _, ok := item["models"]; ok {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("shareView", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "view", false)

		common(email, ownerUsername, projectName, marshal(models))

		common(email2, ownerUsername, projectName, nil)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("TEST: item returned: %#v\n", item)
		if len(item["models"].L) != 5 {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("notShared", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)

		common(email, ownerUsername, projectName, marshal(models))

		common(email2, ownerUsername, projectName, nil)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("TEST: item returned: %#v\n", item)
		if len(item["models"].L) != 5 {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})

}

func check(item map[string]*dynamodb.AttributeValue, email string, t *testing.T) {
	if len(item["models"].L) != 5 {
		t.Fatal()
	}
	if *item["sk"].S != pure.BuildSortKey(email, projectName) {
		t.Fatal()
	}
	if *item["startDate"].S != startDate {
		t.Fatal()
	}
	if *item["dueDate"].S != dueDate {
		t.Fatal()
	}
	if *item["entityStatus"].S != "In Progress" {
		t.Fatal()
	}
	if *item["models"].L[0].M["Energy"].S != "A+" {
		t.Fatal()
	}
	if *item["models"].L[0].M["Capacity"].N != "7" {
		t.Fatal()
	}
	if *item["models"].L[4].M["Energy"].S != "A+++" {
		t.Fatal()
	}
	if *item["models"].L[4].M["Capacity"].N != "8" {
		t.Fatal()
	}

}

func TestGetModelsByParams(t *testing.T) {
	models := getModelsByParams(
		[]string{"belgium"},
		[]string{"capacity", "energy"},
		"washing",
		"top loaders",
		"retail",
		"gms-ai",
		[2]uint16{0, 0},
		[]mydynamo.Rstrs{},
	)
	fmt.Println("amount of models:", len(models))
	fmt.Printf("%#v", models)
	if len(models) == 0 {
		t.Fatal()
	}
}

func TestCalc(t *testing.T) {
	userModels := []userModel{
		userModel{Specs: map[string]string{
			"capacity": "8",
			"energy":   "a++",
		}},
		userModel{Specs: map[string]string{
			"capacity": "7",
			"energy":   "a+",
		}},
		userModel{Specs: map[string]string{
			"capacity": "7",
			"energy":   "a++",
		}},
	}
	ch := make(chan out, 1)
	calc(modelsMock, userModels, ch)
	c := <-ch
	if len(c) != 3 {
		t.Fatal()
	}

	if c[0]["capacity"] != "7" || c[0]["energy"] != "a++" {
		t.Fatal()
	}

	if c[1]["capacity"] != "8" || c[1]["energy"] != "a++" {
		t.Fatal()
	}
	if c[2]["capacity"] != "7" || c[2]["energy"] != "a+" {
		t.Fatal()
	}

}
