NAME=TODO
OUT=/tmp/awsLambdaOut.txt

AWS_PROFILE=gms-ai \
aws lambda invoke \
--function-name $NAME \
--payload '{
    "XName": "Capacity",
    "YName": "RPM",
    "Brands": ["Bosch", "LG"],
    "Models": [
        {
            "Capacity": "6",
            "RPM": "1000",
            "Energy": "A+"
        }
    ]
}' \
--log-type Tail \
$OUT |
grep "LogResult" |
awk --field-separator '"' '{print $4}' |
base64 --decode |
grep --color "$NAME\|$" --text

cat $OUT | python3 -m json.tool
