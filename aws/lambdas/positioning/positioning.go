package main

import (
	"fmt"
	"sort"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type event struct {
	Email       string
	Owner       string
	ProjectName string
	PriceRange  [2]uint16
}

type out struct {
	allModelsFromDB map[string]*mydynamo.Model

	Models                []mydynamo.ModelTheoretical
	Competitors           []mydynamo.Model
	CompetitorsLessRanked []mydynamo.Model
}

func main() {
	lambda.Start(handleRequest)
}

func handleRequest(e event) (out, error) {
	fmt.Println("event:", e)

	o := out{}

	countries, specNames, competitorBrands, category, segment, channel, rstrs, specs, err := getUserData(e)
	if err != nil {
		return o, err
	}
	fmt.Printf(
		"countries: %v, specNames: %v, competitorBrands: %v, specs: %v\n",
		countries, specNames, competitorBrands, specs,
	)

	dmn := pure.Subdomain(e.Email)
	o.getAllReportsFromDatabase(countries, specNames, category, segment, channel, dmn, e.PriceRange, rstrs)
	o.calc(competitorBrands, specs, specNames[0], specNames[1])

	return o, nil
}

func (o *out) getAllReportsFromDatabase(
	countries,
	specNames []string,
	category,
	segment,
	channel,
	dmn string,
	priceRange [2]uint16,
	rstrs []mydynamo.Rstrs) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("ERROR: %v, countries: %v, specNames: %v\n", r, countries, specNames)
			panic(r)
		}
	}()

	o.allModelsFromDB = make(map[string]*mydynamo.Model)
	err := mydynamo.DynamodbClient.QueryPages(
		mydynamo.RprtsQuery(
			countries,
			category,
			segment,
			channel,
			dmn,
			priceRange,
			rstrs),
		func(page *dynamodb.QueryOutput, lastPage bool) bool {
			fmt.Println("Reports from DynamoDB: page size:", len(page.Items))
			for _, item := range page.Items {
				modelName := *item["model"].S
				if _, ok := o.allModelsFromDB[modelName]; ok {
					o.allModelsFromDB[modelName].Value += pure.StrToFloat64(*item["value"].N)
					o.allModelsFromDB[modelName].Units += pure.StrToInt(*item["units"].N)
					continue
				}
				m, err := mydynamo.NewModel(item, specNames)
				if err == nil {
					o.allModelsFromDB[modelName] = &m
				}
			}
			return true
		},
	)
	if err != nil {
		panic(err)
	}
}

func (o *out) calc(competitorBrands []string, specs []mydynamo.Specs, specNameX, specNameY string) {
	fmt.Printf(
		"competitorBrands: %v, specs: %#v, specNameX: %v, specNameY: %v\n",
		competitorBrands, specs, specNameX, specNameY,
	)
	fmt.Printf("Amount of models from DynamoDB: %v\n", len(o.allModelsFromDB))

	var _models []mydynamo.Model
	for _, m := range o.allModelsFromDB {
		_models = append(_models, *m)
	}

	sort.Slice(_models, func(i, j int) bool {
		return _models[i].Value > _models[j].Value
	})

	var queue []mydynamo.Model

	for _, sp := range specs {
		for _, _m := range _models {
			if sp.EqByAllSpecs(_m.Specs) {
				o.add(_m)
			}

			for _, b := range competitorBrands {
				if _m.Brand == b {
					if sp.EqByAllSpecs(_m.Specs) && isUnique(o.Competitors, _m) {
						o.Competitors = append(o.Competitors, _m)
						continue
					}
					if _m.EqByXY(sp, specNameX, specNameY) {
						queue = append(queue, _m)
					}
				}
			}
		}
	}

queue:
	for _, q := range queue {
		for _, c := range o.Competitors {
			if c.ID == q.ID {
				continue queue
			}
		}
		addAltCompetIfUnique(&o.CompetitorsLessRanked, q)
	}

	for _i, m := range o.Models {
		o.Models[_i].AverageSalesValue = m.Value / m.Units
	}
}

func (o *out) add(m mydynamo.Model) {
	for i, s := range o.Models {
		if s.EqByAllSpecs(m.Specs) {
			o.Models[i].Value += int(m.Value)
			o.Models[i].Units += m.Units
			return
		}
	}
	o.Models = append(o.Models, mydynamo.ModelTheoretical{
		Units: m.Units,
		Value: int(m.Value),
		Specs: m.Specs,
	})
}

func getUserData(e event) (countries, specNames, competitorBrands []string, category, segment, channel string, rstrs []mydynamo.Rstrs, specs []mydynamo.Specs, err error) {

	item := mydynamo.GetProject(pure.BuildSortKeyForOwner(e.Email, e.Owner, e.ProjectName))
	fmt.Printf("Item project: %#v\n", item)

	username := pure.ExtractUsername(e.Email)
	shrd := item["shrd"]
	if item == nil ||
		(*item["sk"].S != pure.BuildSortKey(e.Email, e.ProjectName) &&
			(shrd == nil || shrd.M[username] == nil ||
				*shrd.M[username].S != "view" &&
					*shrd.M[username].S != "edit")) {
		fmt.Println("HACKINGAttempt")
		err = fmt.Errorf("HACKINGAttempt")
		return
	}

	countries = aws.StringValueSlice(item["params"].M["Countries"].SS)
	category = *item["params"].M["Category"].S
	segment = *item["params"].M["Segment"].S
	channel = *item["params"].M["Channel"].S

	for _, s := range item["params"].M["Specs"].L {
		specNames = append(specNames, *s.S)
	}

	competitorBrands = aws.StringValueSlice(item["params"].M["Competitors"].SS)

	specs = []mydynamo.Specs{}
	for _, model := range item["models"].L {
		m := make(mydynamo.Specs)
		for k, v := range model.M["Specs"].M {
			if v.S != nil {
				m[k] = *v.S
			} else if v.N != nil {
				m[k] = *v.N
			}
		}
		specs = append(specs, m)
	}

	return
}

func addAltCompetIfUnique(competitorsLessRanked *[]mydynamo.Model, m mydynamo.Model) {
	for _, c := range *competitorsLessRanked {
		if c.Model == m.Model {
			return
		}
	}
	*competitorsLessRanked = append(*competitorsLessRanked, m)
}

func isUnique(competitors []mydynamo.Model, m mydynamo.Model) bool {
	for _, _m := range competitors {
		if _m.Specs.EqByAllSpecs(m.Specs) && _m.Brand == m.Brand {
			return false
		}
	}
	return true
}
