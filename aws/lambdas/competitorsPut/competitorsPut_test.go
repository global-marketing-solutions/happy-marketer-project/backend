package main

import (
	"fmt"
	"testing"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	email       = "vitaly@gms-ai.com"
	email2      = "aaa.bbb@gms-ai.com"
	owner       = pure.ExtractUsername(email)
	projectName = "My project name for go test"
	startDate   = "2020-04-30"
	dueDate     = "2021-12-31"
	competitors = `[
    {
        "Brand": "Bosch",
        "Distribution": "89.6",
        "Model": "B 45",
        "Price": 379.2,
        "Rank": 1,
        "TOS": "29.9",
        "Specs": {
            "Capacity": "8",
            "Energy": "A++"
        }
    },
    {
        "Brand": "Bosch",
        "Distribution": "90",
        "Model": "B 27",
        "Price": 319.2,
        "Rank": 2,
        "TOS": "30",
        "Specs": {
            "Capacity": "7",
            "Energy": "A++"
        }
    },
    {
        "Brand": "Bosch",
        "Distribution": "85",
        "Model": "B 47",
        "Price": 382.8,
        "Rank": 7,
        "TOS": "28.5",
        "Specs": {
            "Capacity": "8",
            "Energy": "A+++"
        }
    },
    {
        "Brand": "Bosch",
        "Distribution": "88.8",
        "Model": "B 29",
        "Price": 322.8,
        "Rank": 10,
        "TOS": "29.6",
        "Specs": {
            "Capacity": "7",
            "Energy": "A+++"
        }
    },
    {
        "Brand": "Bosch",
        "Distribution": "87.8",
        "Model": "B 25",
        "Price": 315.6,
        "Rank": 16,
        "TOS": "29.3",
        "Specs": {
            "Capacity": "7",
            "Energy": "A+"
        }
    }
]`
)

func init() {
	println("init...")
	mydynamo.ProjectDelete(email, projectName)
	mydynamo.ProjectDelete(email2, projectName)
}

func TestCompetitorsPut(t *testing.T) {
	t.Run("owner", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)

		competitorsPut(email, owner, projectName, competitors)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		fmt.Printf("TEST item: %#v\n", item)
		check(item, email, t)

		mydynamo.ProjectDelete(email, projectName)
	})

	t.Run("shareEdit", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "edit", false)

		competitorsPut(email2, owner, projectName, competitors)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		check(item, email, t)

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("shareEditOwnerWithDot", func(t *testing.T) {
		mydynamo.AddProject(email2, projectName, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email2, projectName), pure.ExtractUsername(email), "edit", false)

		competitorsPut(email, pure.ExtractUsername(email2), projectName, competitors)

		item := mydynamo.GetProject(pure.BuildSortKey(email2, projectName))
		fmt.Printf("TEST item: %#v\n", item)
		check(item, email2, t)

		mydynamo.ProjectDelete(email2, projectName)
	})
	t.Run("shareView", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, projectName), pure.ExtractUsername(email2), "view", false)

		competitorsPut(email2, owner, projectName, competitors)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		if item["competitors"] != nil {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("notShared", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)

		competitorsPut(email2, owner, projectName, competitors)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		if item["competitors"] != nil {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})
	t.Run("itemNotExists", func(t *testing.T) {
		competitorsPut(email, owner, projectName, competitors)

		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		if len(item) > 0 {
			t.Fatal()
		}
	})
	t.Run("projectStatusAltered", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		mydynamo.Complete(email, owner, projectName)

		competitorsPut(email, owner, projectName, competitors)
		item := mydynamo.GetProject(pure.BuildSortKey(email, projectName))
		check(item, email, t)

		mydynamo.ProjectDelete(email, projectName)
	})
}

func check(item map[string]*dynamodb.AttributeValue, email string, t *testing.T) {
	if len(item["competitors"].L) != 5 {
		t.Fatal()
	}

	brand := *item["competitors"].L[0].M["Brand"].S
	if brand != "Bosch" {
		t.Fatalf("Got: %v\n", brand)
	}

	// TODO but Distribution must be a number?
	distribution := *item["competitors"].L[0].M["Distribution"].S
	if distribution != "89.6" {
		t.Fatalf("Got: %v\n", distribution)
	}

	model := *item["competitors"].L[0].M["Model"].S
	if model != "B 45" {
		t.Fatalf("Got: %v\n", model)
	}

	price := *item["competitors"].L[0].M["Price"].N
	if price != "379.2" {
		t.Fatalf("Got: %v\n", price)
	}

	rank := *item["competitors"].L[0].M["Rank"].N
	if rank != "1" {
		t.Fatalf("Got: %v\n", rank)
	}

	// TODO but TOS must be a number?
	tos := *item["competitors"].L[0].M["TOS"].S
	if tos != "29.9" {
		t.Fatalf("Got: %v\n", tos)
	}

	actual := *item["sk"].S
	expected := pure.BuildSortKey(email, projectName)
	if actual != expected {
		panic(fmt.Sprintf(`Actual: "%v", expected: "%v"\n`, actual, expected))
	}

	if *item["entityStatus"].S != "In Progress" {
		t.Fatal()
	}
}
