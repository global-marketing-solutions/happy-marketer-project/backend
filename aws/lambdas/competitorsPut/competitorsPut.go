package main

import (
	"context"
	"encoding/json"
	"fmt"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var (
	dynamodbClient = dynamodb.New(mydynamo.Sess)

	updateItemInput = dynamodb.UpdateItemInput{
		TableName: aws.String(mydynamo.TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":status": {
				S: aws.String("In Progress"),
			},
		},
	}

	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	projectName := pure.DecodePercent(r.PathParameters["name"])
	owner := r.QueryStringParameters["owner"]

	competitorsPut(email, owner, projectName, r.Body)

	return resp, nil
}

func competitorsPut(email, owner, projectName, competitors string) {
	sk := pure.BuildSortKeyForOwner(email, owner, projectName)
	updateItemInput.Key["sk"] = &dynamodb.AttributeValue{
		S: &sk,
	}

	updateItemInput.ExpressionAttributeValues = map[string]*dynamodb.AttributeValue{
		":status": {
			S: aws.String("In Progress"),
		},
		":sk": {
			S: aws.String(pure.BuildSortKey(email, projectName)),
		},
		":mode": {
			S: aws.String("edit"),
		},
	}

	status := "entityStatus = :status"

	if len(competitors) == 0 {
		updateItemInput.UpdateExpression = aws.String("REMOVE competitors SET " + status)
	} else {
		updateItemInput.UpdateExpression = aws.String("SET competitors = :competitors, " + status)
		updateItemInput.ExpressionAttributeValues[":competitors"] = &dynamodb.AttributeValue{
			L: marshal(competitors).L,
		}
	}

	updateItemInput.ConditionExpression = aws.String(mydynamo.ConditionExpression())
	updateItemInput.ExpressionAttributeNames = map[string]*string{
		"#username": aws.String(pure.ExtractUsername(email)),
	}

	fmt.Printf("competitorsPut(): updateItemInput before using: %#v\n", updateItemInput)
	_, err := dynamodbClient.UpdateItem(&updateItemInput)
	if err != nil {
		mydynamo.ErrCheck(err)
	}
}

func marshal(data string) *dynamodb.AttributeValue {
	var sl []map[string]interface{}
	err := json.Unmarshal([]byte(data), &sl)
	if err != nil {
		panic(err)
	}
	attr, err := dynamodbattribute.Marshal(sl)
	if err != nil {
		panic(err)
	}

	fmt.Printf("marshal() resp: %#v\n", attr)

	return attr
}
