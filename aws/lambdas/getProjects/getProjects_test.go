package main

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func TestIsAvailable(t *testing.T) {

	t.Run("match", func(t *testing.T) {
		if !isAvailable(
			"vitaly@gms-ai.com",
			"gms-ai.com vitaly My project name",
			"",
			"In Progress",
			&dynamodb.AttributeValue{},
		) {
			t.Fatal()
		}
	})

	t.Run("notMatch", func(t *testing.T) {
		if isAvailable(
			"vitaly@gms-ai.com",
			"gms-ai.com taras My project name",
			"",
			"In Progress",
			&dynamodb.AttributeValue{},
		) {
			t.Fatal()
		}
	})

	t.Run("notMatchBecauseOfStatus", func(t *testing.T) {
		if isAvailable(
			"vitaly@gms-ai.com",
			"gms-ai.com vitaly My project name",
			"",
			"Archived",
			&dynamodb.AttributeValue{},
		) {
			t.Fatal()
		}
	})

	t.Run("matchWithStatus", func(t *testing.T) {
		if !isAvailable(
			"vitaly@gms-ai.com",
			"gms-ai.com vitaly My project name",
			"Archived",
			"Archived",
			&dynamodb.AttributeValue{},
		) {
			t.Fatal()
		}
	})

	t.Run("matchWithShared", func(t *testing.T) {
		if !isAvailable(
			"vitaly@gms-ai.com",
			"gms-ai.com taras My project name",
			"",
			"In Progress",
			&dynamodb.AttributeValue{
				M: map[string]*dynamodb.AttributeValue{
					"vitaly": &dynamodb.AttributeValue{S: aws.String("view")},
				},
			},
		) {
			t.Fatal()
		}
	})

	t.Run("notMatchWithSharedButWrongStatus", func(t *testing.T) {
		if isAvailable(
			"vitaly@gms-ai.com",
			"gms-ai.com taras My project name",
			"",
			"Archived",
			&dynamodb.AttributeValue{
				M: map[string]*dynamodb.AttributeValue{
					"vitaly": &dynamodb.AttributeValue{S: aws.String("view")},
				},
			},
		) {
			t.Fatal()
		}
	})

}

func TestIsAccessMatch(t *testing.T) {

	t.Run("skMatch", func(t *testing.T) {
		if !isAccessMatch("taras@gms-ai.com", "gms-ai.com taras My project name", &dynamodb.AttributeValue{}) {
			t.Fatal()
		}
	})

	t.Run("skMatch", func(t *testing.T) {
		if !isAccessMatch("a.b@gms-ai.com", "gms-ai.com a.b My project name", &dynamodb.AttributeValue{}) {
			t.Fatal()
		}
	})

	t.Run("skNotMatch", func(t *testing.T) {
		if isAccessMatch("taras@gms-ai.com", "gms-ai.com vitaly My project name", &dynamodb.AttributeValue{}) {
			t.Fatal()
		}
	})

	t.Run("skNotMatch", func(t *testing.T) {
		if isAccessMatch("a.b@gms-ai.com", "gms-ai.com vitaly My project name", &dynamodb.AttributeValue{}) {
			t.Fatal()
		}
	})

	t.Run("skNotMatch", func(t *testing.T) {
		if isAccessMatch("a.b@gms-ai.com", "gms-ai.com c.d My project name", &dynamodb.AttributeValue{}) {
			t.Fatal()
		}
	})

	t.Run("skNotMatchWithAttributes", func(t *testing.T) {
		if isAccessMatch(
			"taras@gms-ai.com",
			"gms-ai.com vitaly My project name",
			&dynamodb.AttributeValue{
				M: map[string]*dynamodb.AttributeValue{
					"marina": &dynamodb.AttributeValue{S: aws.String("view")},
				},
			},
		) {
			t.Fatal()
		}
	})

	t.Run("skMatchWithAttributes", func(t *testing.T) {
		if !isAccessMatch(
			"taras@gms-ai.com",
			"gms-ai.com vitaly My project name",
			&dynamodb.AttributeValue{
				M: map[string]*dynamodb.AttributeValue{
					"taras": &dynamodb.AttributeValue{S: aws.String("view")},
				},
			},
		) {
			t.Fatal()
		}
	})

	t.Run("skMatchWithAttributes", func(t *testing.T) {
		if !isAccessMatch(
			"taras@gms-ai.com",
			"gms-ai.com vitaly My project name",
			&dynamodb.AttributeValue{
				M: map[string]*dynamodb.AttributeValue{
					"taras": &dynamodb.AttributeValue{S: aws.String("edit")},
				},
			},
		) {
			t.Fatal()
		}
	})

}

func TestIsStatusMatch(t *testing.T) {

	t.Run("emptyToInProgress", func(t *testing.T) {
		if !isStatusMatch("", "In Progress") {
			t.Fatal()
		}
	})

	t.Run("emptyToComplete", func(t *testing.T) {
		if !isStatusMatch("", "Completed") {
			t.Fatal()
		}
	})

	t.Run("emptyToArchive", func(t *testing.T) {
		if isStatusMatch("", "Archived") {
			t.Fatal()
		}
	})

	t.Run("archived", func(t *testing.T) {
		if !isStatusMatch("Archived", "Archived") {
			t.Fatal()
		}
	})
}
