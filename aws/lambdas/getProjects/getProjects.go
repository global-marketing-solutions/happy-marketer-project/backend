package main

import (
	"fmt"

	"../common/isAvailable"
	"../common/mycognito"
	"../common/pure"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

var (
	sess         = session.Must(session.NewSession())
	dynamoClient = dynamodb.New(sess)

	// https://docs.aws.amazon.com/sdk-for-go/api/service/dynamodb/#QueryInput
	queryInput = &dynamodb.QueryInput{
		TableName: aws.String("marketegy"),
	}

	keyCondBuilder expression.KeyConditionBuilder

	condBuilder expression.ConditionBuilder

	expr expression.Expression

	loginsToHumanNamesCache map[string]string
)

type event struct {
	Email  string
	Status string
}

type params struct {
	Brand, Category, Channel, Currency, Segment string
	Models, Price, MsrpVsMgp                    int
	PriceIndexRange                             *struct {
		Value   [2]uint16
		Percent float64
	} `json:",omitempty"`
	Restrictions                  *[]map[string]interface{}
	Competitors, Countries, Specs []string

	SelectedStateIds *struct {
		Step2 []string
		Step3 map[string][]string
	}
}

type model struct {
	Specs, Sums map[string]string `json:",omitempty"`
	Fin         map[string]int    `json:",omitempty"`
	Price, Msrp int               `json:",omitempty"`
}

type competitor struct {
	Brand, Distribution, ID, Model, Price, TOS string
	ChangedOnRow                               uint8
	Specs                                      map[string]string
}

type businesscase struct {
	Financials, Specs []string
	Kpi, Period       string
}

type summary struct {
	SelFinParams, SelKpi, SelSpecs []string
}

type project struct {
	Name string

	OwnerUsername string
	OwnerName     string

	Shrd      map[string]map[string]string `json:",omitempty"`
	StartDate string
	DueDate   string
	Status    string

	Params       *params       `json:",omitempty"`
	Models       []model       `json:",omitempty"`
	Competitors  []competitor  `json:",omitempty"`
	Businesscase *businesscase `json:",omitempty"`
	Summary      *summary      `json:",omitempty"`
}

func init() {
	mycognito.InitWithSession(sess)
}

func main() {
	lambda.Start(handleRequest)
}
func handleRequest(e event) (projects []project, err error) {
	fmt.Println("email:", e.Email)

	keyCondBuilder = expression.Key("pk").Equal(expression.Value("project")).And(
		expression.Key("sk").BeginsWith(pure.ExtractDomain(e.Email) + " "))

	if e.Status == "Archived" {
		condBuilder = expression.Name("entityStatus").Equal(expression.Value("Archived"))
	} else {
		condBuilder = expression.Or(expression.Name("entityStatus").Equal(expression.Value("In Progress")),
			expression.Name("entityStatus").Equal(expression.Value("Completed")))
	}

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondBuilder).WithFilter(condBuilder).Build()
	if err != nil {
		panic(err)
	}

	queryInput.KeyConditionExpression = expr.KeyCondition()
	queryInput.ExpressionAttributeNames = expr.Names()
	queryInput.ExpressionAttributeValues = expr.Values()
	queryInput.FilterExpression = expr.Filter()

	fmt.Println("queryInput:", queryInput)
	queryOutput, err := dynamoClient.Query(queryInput)
	//fmt.Println("queryOutput:", queryOutput)
	if err != nil {
		panic(err)
	}

	for _, item := range queryOutput.Items {
		if isAvailable.IsAccessMatch(e.Email, *item["sk"].S, item["shrd"]) {
			p := project{
				Name: pure.ProjectName(*item["sk"].S),

				OwnerUsername: pure.Username(*item["sk"].S),
				OwnerName:     mycognito.GetHumanName(pure.Email(*item["sk"].S)),

				StartDate: *item["startDate"].S,
				DueDate:   *item["dueDate"].S,
				Status:    *item["entityStatus"].S,
			}
			if item["shrd"] != nil {
				shrd := make(map[string]string)
				dynamodbattribute.Unmarshal(item["shrd"], &shrd)
				p.Shrd = make(map[string]map[string]string)
				for login, mode := range shrd {
					p.Shrd[login] = map[string]string{
						"humanName": mycognito.GetHumanName(login + "@" + pure.ExtractDomain(e.Email)),
						"mode":      mode,
					}
				}
			}
			if item["params"] != nil {
				dynamodbattribute.Unmarshal(item["params"], &p.Params)
			}
			if item["models"] != nil {
				dynamodbattribute.Unmarshal(item["models"], &p.Models)
			}
			if item["competitors"] != nil {
				dynamodbattribute.Unmarshal(item["competitors"], &p.Competitors)
			}
			if item["businesscase"] != nil {
				dynamodbattribute.Unmarshal(item["businesscase"], &p.Businesscase)
			}
			if item["summary"] != nil {
				dynamodbattribute.Unmarshal(item["summary"], &p.Summary)
			}

			projects = append(projects, p)
		}

	}
	return
}
