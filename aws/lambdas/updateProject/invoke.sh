NAME=updateProject
OUT=/tmp/awsLambdaOut.txt

AWS_PROFILE=gms-ai \
aws lambda invoke \
--function-name $NAME \
--payload '{
    "email": "vitaly@gms-ai.com",
    "name": "My project",
    "isRenamed": false,
    "responsible": "Some guy from lambda",
    "startDate": "2020-11-21",
    "dueDate": "2020-02-22"
}' \
--log-type Tail \
$OUT |
grep "LogResult" |
awk --field-separator '"' '{print $4}' |
base64 --decode |
grep --color "$NAME\|$" --text

cat $OUT | python3 -m json.tool
