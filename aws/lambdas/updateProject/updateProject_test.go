package main

import (
	"fmt"
	"testing"

	"../common/mydynamo"
	"../common/pure"
)

const (
	email     = "vitaly@gms-ai.com"
	email2    = "vitaliy.sichkar@gms-ai.com"
	name      = "My project name from local go test"
	startDate = "2020-04-04"
	dueDate   = "2021-01-01"
	status    = "In Progress"
)

func init() {
	mydynamo.ProjectDelete(email, name)
}

func TestUpdateItem(t *testing.T) {
	mydynamo.ProjectDelete(email, name)

	t.Run("dates", func(t *testing.T) {
		mydynamo.AddProject(email, name, "2000-01-01", "2030-01-01")
		updateItem(email, "", name, startDate, dueDate)

		item := mydynamo.GetProject(email, name)
		fmt.Printf("TEST: got item after altering dates: %#v\n", item)

		if *item["startDate"].S != startDate {
			t.Fatalf("startDate: %v\n", *item["startDate"].S)
		}
		fmt.Println("startDate is ok")

		if *item["dueDate"].S != dueDate {
			t.Fatalf("dueDate: %v\n", item["dueDate"])
		}
		if item["shared"] != nil {
			t.Fatalf("shared: %#v\n", item["shared"])
		}

		mydynamo.ProjectDelete(email, name)
	})

	t.Run("datesShared", func(t *testing.T) {
		mydynamo.AddProject(email, name, "2000-01-01", "2030-01-01")
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), pure.ExtractUsername(email2), "edit", false)
		updateItem(email2, pure.ExtractUsername(email), name, startDate, dueDate)

		item := mydynamo.GetProject(email, name)
		fmt.Printf("TEST: got item after altering dates: %#v\n", item)
		if *item["startDate"].S != startDate {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})

	t.Run("hack", func(t *testing.T) {
		startDateBefore, dueDateBefore := "2000-01-01", "2030-01-01"
		mydynamo.AddProject(email, name, startDateBefore, dueDateBefore)
		updateItem(email2, pure.ExtractUsername(email), name, startDate, dueDate)

		item := mydynamo.GetProject(email, name)
		fmt.Printf("TEST: got item after trying to altere dates: %#v\n", item)
		if *item["startDate"].S == startDate {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})

}

func TestUpdateItemWithCopy(t *testing.T) {
	nameNew := name + ": renamed"
	t.Run("ownerRename", func(t *testing.T) {
		mydynamo.AddProject(email, name, "2000-01-01", "2030-01-01")

		updateItemWithCopy(email, "", name, nameNew, startDate, dueDate)

		item := mydynamo.GetProject(email, nameNew)
		fmt.Printf("Item renamed: %#v\n", item)

		if *item["sk"].S != pure.BuildSortKey(email, nameNew) {
			t.Fatalf("sk: %v\n", item["sk"])
		}
		if *item["startDate"].S != startDate {
			t.Fatalf("startDate actual: %v\n", item["startDate"])
		}
		if *item["dueDate"].S != dueDate {
			t.Fatalf("dueDate actual: %v\n", item["dueDate"])
		}
		if *item["entityStatus"].S != status {
			t.Fatalf("entityStatus actual: %v\n", item["entityStatus"])
		}

		item = mydynamo.GetProject(email, name)
		if item != nil {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, nameNew)
	})

	t.Run("shareeRename", func(t *testing.T) {
		mydynamo.AddProject(email, name, "2000-01-01", "2030-01-01")
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), pure.ExtractUsername(email2), "edit", false)
		updateItemWithCopy(email2, pure.ExtractUsername(email), name, nameNew, startDate, dueDate)

		item := mydynamo.GetProject(email, nameNew)
		fmt.Printf("Item renamed (I hope): %#v\n", item)

		if *item["sk"].S != pure.BuildSortKey(email, nameNew) {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, nameNew)
	})

	t.Run("shareeViewMode", func(t *testing.T) {
		mydynamo.AddProject(email, name, "2000-01-01", "2030-01-01")
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), pure.ExtractUsername(email2), "view", false)
		updateItemWithCopy(email2, pure.ExtractUsername(email), name, nameNew, startDate, dueDate)

		item := mydynamo.GetProject(email, nameNew)
		fmt.Printf("Item by new project name (must be empty): %#v\n", item)

		if item != nil {
			t.Fatal()
		}

		item = mydynamo.GetProject(email, name)
		fmt.Printf("Item after trying to rename (must remain original name): %#v\n", item)

		if *item["sk"].S != pure.BuildSortKey(email, name) {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})

}
