package main

import (
	"context"
	"encoding/json"
	"fmt"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

const (
	tableName = "marketegy"
)

var (
	cogn = cognitoidentityprovider.New(mydynamo.Sess)

	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

type event struct {
	NameNew       string
	OwnerUsername string

	StartDate string
	DueDate   string
}

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	nameCurrent := pure.DecodePercent(r.PathParameters["name"])

	var e event
	json.Unmarshal([]byte(r.Body), &e)

	fmt.Printf("event: %#v; nameCurrent: %v\n", e, nameCurrent)

	if len(e.NameNew) > 0 {
		updateItemWithCopy(email, e.OwnerUsername, nameCurrent, e.NameNew, e.StartDate, e.DueDate)
		return resp, nil
	}

	updateItem(email, e.OwnerUsername, nameCurrent, e.StartDate, e.DueDate)

	return resp, nil
}

func updateItem(email, ownerUsername, name, startDate, dueDate string) {
	var update expression.UpdateBuilder
	if len(startDate) > 0 {
		update = expression.Set(
			expression.Name("startDate"),
			expression.Value(startDate),
		)
	}
	if len(dueDate) > 0 {
		update = update.Set(
			expression.Name("dueDate"),
			expression.Value(dueDate),
		)
	}

	fmt.Printf("expression.UpdateBuilder: %#v\n", update)
	expr, err := expression.NewBuilder().WithUpdate(update).Build()
	if err != nil {
		panic(err)
	}

	updateItemInput := &dynamodb.UpdateItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("project"),
			},
			"sk": {
				S: aws.String(pure.BuildSortKey(email, name)),
			},
		},
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	if ownerUsername == "" || ownerUsername == pure.ExtractUsername(email) {
		mydynamo.UpdateItemIfExists(updateItemInput)
	} else {
		sk := pure.BuildSortKey(ownerUsername+"@"+pure.ExtractDomain(email), name)
		updateItemInput.Key["sk"].S = &sk
		mydynamo.UpdateItemIfSharedEdit(updateItemInput, pure.ExtractUsername(email))
	}
}

func updateItemWithCopy(email, ownerUsername, nameCurrent, nameNew, startDate, dueDate string) {
	emailOwner := email
	if ownerUsername != "" && ownerUsername != pure.ExtractUsername(email) {
		emailOwner = ownerUsername + "@" + pure.ExtractDomain(email)
	}
	item := mydynamo.GetProject(pure.BuildSortKey(emailOwner, nameCurrent))
	fmt.Printf("item original before altering: %#v\n", item)

	if item == nil ||
		(item["shrd"] == nil && emailOwner != email) ||
		(item["shrd"] != nil && item["shrd"].M[pure.ExtractUsername(email)] == nil) ||
		(item["shrd"] != nil && *item["shrd"].M[pure.ExtractUsername(email)].S == "view") {
		fmt.Println("HACKING attempt")
		return
	}

	item["sk"].S = aws.String(pure.BuildSortKey(emailOwner, nameNew))

	if len(startDate) > 0 {
		item["startDate"].S = &startDate
	}
	if len(dueDate) > 0 {
		item["dueDate"].S = &dueDate
	}

	fmt.Printf("PutItem input: %#v\n", item)
	mydynamo.PutItem(item)
	mydynamo.ProjectDelete(emailOwner, nameCurrent)
}
