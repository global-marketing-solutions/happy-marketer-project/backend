package main

import (
	"context"
	"encoding/json"
	"strings"

	"../common/mycognito"
	"../common/mydynamo"
	"../common/myses"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	resp = events.APIGatewayProxyResponse{
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

type event struct {
	Shrd   map[string]string
	IsShrd bool
}

func init() {
	mycognito.InitWithSession(mydynamo.Sess)
	myses.InitWithSession(mydynamo.Sess)
}

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)

	var e event
	json.Unmarshal([]byte(r.Body), &e)
	usernameOfSharee, mode := pure.Unpack(e.Shrd)
	emailOfSharee := usernameOfSharee + "@" + pure.ExtractDomain(email)
	if !mycognito.IsUserExists(emailOfSharee) {
		resp.StatusCode = 400
		return resp, nil
	}
	entityName := pure.DecodePercent(r.PathParameters["name"])

	if mode == "view" || mode == "edit" {
		mydynamo.EntityShare(extractType(r.Path), pure.BuildSortKey(email, entityName), usernameOfSharee, mode, e.IsShrd)
	}

	if mode == "admin" {
		updateItemWithCopy(extractType(r.Path), email, emailOfSharee, entityName)
	}

	// TODO verify domain for sending emails
	emailToUser(email, emailOfSharee, entityName)

	resp.StatusCode = 200
	resp.Body = mycognito.GetHumanName(emailOfSharee)
	return resp, nil
}

func updateItemWithCopy(_type, email, emailOfSharee, entityName string) {
	item := getEntity(_type, pure.BuildSortKey(email, entityName))

	item["sk"].S = aws.String(pure.BuildSortKey(emailOfSharee, entityName))

	if _, ok := item["shrd"]; !ok {
		item["shrd"] = &dynamodb.AttributeValue{M: make(map[string]*dynamodb.AttributeValue)}
	}
	item["shrd"].M[pure.ExtractUsername(email)] = &dynamodb.AttributeValue{
		S: aws.String("edit"),
	}
	delete(item["shrd"].M, pure.ExtractUsername(emailOfSharee))

	mydynamo.PutItem(item)
	mydynamo.EntityDelete(_type, email, entityName)
}

func extractType(path string) string {
	if strings.HasPrefix(path, "/projects/") {
		return "project"
	}
	return "document"
}

func getEntity(_type, sk string) map[string]*dynamodb.AttributeValue {
	getItemInput := dynamodb.GetItemInput{
		TableName: aws.String("marketegy"),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String(_type),
			},
			"sk": {
				S: &sk,
			},
		},
	}
	getItemOutput, err := mydynamo.DynamodbClient.GetItem(&getItemInput)
	if err != nil {
		panic(err)
	}
	return getItemOutput.Item
}

func emailToUser(email, emailOfSharee, entityName string) {
	subject := "Marketegy: entity has been shared with you"
	msg := "User " + email + " shared an entity with you: " + entityName + "."
	msg += "\n\nYou can check your entities at https://marketegy.gms-ai.com"
	myses.Send(emailOfSharee, subject, msg)
}
