package main

import (
	"fmt"
	"testing"

	"../common/mydynamo"
	"../common/pure"
)

const (
	email          = "vitaly@gms-ai.com"
	emailOfSharee  = "maxim@gms-ai.com"
	emailOfSharee2 = "anna@gms-ai.com"
	projectName    = "My project name from go test of share.go"

	startDate = "2000-01-01"
	dueDate   = "2030-01-01"

	entityStatus = "In Progress"
)

func init() {
	mydynamo.ProjectDelete(email, projectName)
}

func TestUpdateItemWithCopy(t *testing.T) {
	t.Run("ownerTransfership", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)

		updateItemWithCopy(email, emailOfSharee, projectName)

		item := mydynamo.GetProject(pure.BuildSortKey(emailOfSharee, projectName))
		fmt.Printf("Item with another owner: %#v\n", item)

		if *item["sk"].S != pure.BuildSortKey(emailOfSharee, projectName) {
			t.Fatalf("sk: %v\n", item["sk"])
		}
		if *item["startDate"].S != startDate {
			t.Fatalf("startDate actual: %v\n", item["startDate"])
		}
		if *item["dueDate"].S != dueDate {
			t.Fatalf("dueDate actual: %v\n", item["dueDate"])
		}
		if *item["entityStatus"].S != entityStatus {
			t.Fatalf("entityStatus actual: %v\n", item["entityStatus"])
		}
		if *item["shrd"].M[pure.ExtractUsername(email)].S != "edit" {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, projectName)
	})

	t.Run("ownerTransfershipWithRemovingNewOwnerFromShrd", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		sk := pure.BuildSortKey(email, projectName)
		mydynamo.ProjectShare(sk, pure.ExtractUsername(emailOfSharee), "view", false)

		updateItemWithCopy(email, emailOfSharee, projectName)

		item := mydynamo.GetProject(pure.BuildSortKey(emailOfSharee, projectName))
		fmt.Printf("Item with another owner and empty shrd: %#v\n", item)

		if item["shrd"].M[pure.ExtractUsername(emailOfSharee)] != nil {
			t.Fatal()
		}

		mydynamo.ProjectDelete(emailOfSharee, projectName)
	})

	t.Run("ownerTransfershipWithRemovingNewOwnerFromShrdWithMultipleShrd", func(t *testing.T) {
		mydynamo.AddProject(email, projectName, startDate, dueDate)
		sk := pure.BuildSortKey(email, projectName)
		mydynamo.ProjectShare(sk, pure.ExtractUsername(emailOfSharee), "view", false)
		mydynamo.ProjectShare(sk, pure.ExtractUsername(emailOfSharee2), "edit", false)

		updateItemWithCopy(email, emailOfSharee, projectName)

		item := mydynamo.GetProject(pure.BuildSortKey(emailOfSharee, projectName))
		fmt.Printf("Item with another owner and updated shrd: %#v\n", item)

		if len(item["shrd"].M) != 2 {
			t.Fatal()
		}

		if item["shrd"].M[pure.ExtractUsername(emailOfSharee)] != nil {
			t.Fatal()
		}
		if *item["shrd"].M[pure.ExtractUsername(emailOfSharee2)].S != "edit" {
			t.Fatal()
		}

		mydynamo.ProjectDelete(emailOfSharee, projectName)
	})
}
