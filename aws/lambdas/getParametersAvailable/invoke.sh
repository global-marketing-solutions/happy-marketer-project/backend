NAME=getParametersAvailable
OUT=/tmp/awsLambdaOut.txt

AWS_PROFILE=gms-ai \
aws lambda invoke \
--function-name $NAME \
--payload '' --log-type Tail \
$OUT |
grep "LogResult" |
awk --field-separator '"' '{print $4}' |
base64 --decode |
grep --color "$NAME\|$" --text

cat $OUT | python3 -m json.tool
