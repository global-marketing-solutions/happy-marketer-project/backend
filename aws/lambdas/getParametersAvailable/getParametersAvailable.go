package main

import (
	"fmt"
	"math"

	"../common/pure"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	sess           = session.Must(session.NewSession())
	dynamodbClient = dynamodb.New(sess)

	nonSpecNames = map[string]struct{}{
		"pk":           {},
		"sk":           {},
		"brand":        {},
		"model":        {},
		"category":     {},
		"channel":      {},
		"country":      {},
		"currency":     {},
		"distribution": {},
		"group":        {},
		"price":        {},
		"segment":      {},
		"tos":          {},
		"units":        {},
		"value":        {},
		"year":         {},
	}

	// https://docs.aws.amazon.com/sdk-for-go/api/service/dynamodb/#DynamoDB.QueryPages
	queryInput = &dynamodb.QueryInput{
		TableName:              aws.String("marketegy"),
		KeyConditionExpression: aws.String("pk = :pk"),
	}
)

type event struct {
	Email string

	Countries []string
	Category  string
	Segment   string
	Channel   string
}

type specsAllPossible map[string][]interface{}

type priceIndexRange struct {
	Min     uint16
	Max     uint16
	Percent float64
}

type out struct {
	Countries []string
	countries map[string]struct{}

	Categories []string
	categories map[string]struct{}

	Segments []string
	segments map[string]struct{}

	Channels []string
	channels map[string]struct{}

	Currencies []string
	currencies map[string]map[string]struct{}

	Competitors []string
	competitors map[string]struct{}

	SpecsAllPossible specsAllPossible

	PriceIndexRange priceIndexRange
}

func (o *out) addCountry(country string) {
	if o.countries == nil {
		o.countries = map[string]struct{}{}
	}
	o.countries[country] = struct{}{}
}

func (o *out) addCategory(item map[string]*dynamodb.AttributeValue, countries []string) {
	if countries[0] == "all" {
		goto category
	}
	for _, country := range countries {
		if country == *item["country"].S {
			goto category
		}
	}
	return

category:
	if o.categories == nil {
		o.categories = map[string]struct{}{}
	}
	o.categories[*item["category"].S] = struct{}{}
}
func (o *out) addSegment(item map[string]*dynamodb.AttributeValue, countries []string, category string) {
	if countries[0] == "all" {
		goto category
	}
	for _, country := range countries {
		if country == *item["country"].S {
			goto category
		}
	}
	return

category:
	if category != *item["category"].S {
		return
	}

	if o.segments == nil {
		o.segments = map[string]struct{}{}
	}
	o.segments[*item["segment"].S] = struct{}{}
}
func (o *out) addChannel(item map[string]*dynamodb.AttributeValue, countries []string, category, segment string) bool {
	if countries[0] == "all" {
		goto category
	}
	for _, country := range countries {
		if country == *item["country"].S {
			goto category
		}
	}
	return false

category:
	if category != *item["category"].S {
		return false
	}

	if segment != *item["segment"].S {
		return false
	}

	if o.channels == nil {
		o.channels = map[string]struct{}{"all": struct{}{}}
	}
	o.channels[*item["channel"].S] = struct{}{}

	return true
}

func (o *out) addCompetitor(countries []string, category, segment string, item map[string]*dynamodb.AttributeValue) {
	for _, c := range countries {
		if c == *item["country"].S {
			goto next
		}
	}
	return

next:
	if category == *item["category"].S && segment == *item["segment"].S {
		if o.competitors == nil {
			o.competitors = map[string]struct{}{}
		}
		o.competitors[*item["brand"].S] = struct{}{}
	}
}

func (o *out) addCurrency(country, currency string) {
	if o.currencies == nil {
		o.currencies = map[string]map[string]struct{}{
			country: map[string]struct{}{
				currency: struct{}{},
			},
		}
		return
	}
	if o.currencies[country] == nil {
		o.currencies[country] = map[string]struct{}{currency: struct{}{}}
		return
	}
	o.currencies[country][currency] = struct{}{}
}

func (o *out) addSpecValue(item map[string]*dynamodb.AttributeValue, countries []string, category, segment, channel string) {
	if len(countries) == 0 || countries[0] == "all" {
		goto category
	}
	for _, country := range countries {
		if country == *item["country"].S {
			goto category
		}
	}
	return

category:
	if category != "" && category != *item["category"].S {
		return
	}

	if segment != "" && segment != *item["segment"].S {
		return
	}

	if channel != "" && channel != *item["channel"].S && channel != "all" {
		return
	}

	for specName, attributeValue := range item {
		if _, ok := nonSpecNames[specName]; ok {
			continue
		}
		v := getSpecValue(specName, attributeValue)
		o.SpecsAllPossible.addIfNotAdded(specName, v)
	}
}

func (s specsAllPossible) addIfNotAdded(k string, v interface{}) {
	for _, _v := range s[k] {
		if _v == v {
			return
		}
	}
	s[k] = append(s[k], v)
}

func getSpecValue(specName string, v *dynamodb.AttributeValue) interface{} {
	if v.S != nil {
		return *v.S
	}
	if v.N != nil {
		return pure.StrToInt(*v.N)
	}
	panic("No value in AttributeValue: " + v.String())
}

func (o out) transcode() out {
	for k := range o.categories {
		o.Categories = append(o.Categories, k)
	}
	for k := range o.segments {
		o.Segments = append(o.Segments, k)
	}
	for k := range o.channels {
		o.Channels = append(o.Channels, k)
	}
	for k := range o.countries {
		o.Countries = append(o.Countries, k)
	}
	for k := range o.competitors {
		o.Competitors = append(o.Competitors, k)
	}
	for con := range o.countries {
		for cur := range o.currencies[con] {
			o.Currencies = append(o.Currencies, cur)
		}
	}
	return o
}

func main() {
	lambda.Start(handleRequest)
}
func handleRequest(event event) (out, error) {
	o := out{}

	o.SpecsAllPossible = make(map[string][]interface{})

	queryInput.SetExpressionAttributeValues(
		map[string]*dynamodb.AttributeValue{
			":pk": {
				S: aws.String("report " + pure.Subdomain(event.Email)),
			},
		},
	)

	var values, units float64
	var prices []float64

	err := dynamodbClient.QueryPages(
		queryInput,
		func(page *dynamodb.QueryOutput, lastPage bool) bool {
			for _, item := range page.Items {
				o.addCompetitor(event.Countries, event.Category, event.Segment, item)

				o.addCountry(*item["country"].S)

				for _, con := range event.Countries {
					if con == *item["country"].S {
						o.addCurrency(con, *item["currency"].S)
					}
				}

				if len(event.Countries) == 0 {
					continue
				}

				o.addCategory(item, event.Countries)
				if event.Category == "" {
					continue
				}

				o.addSegment(item, event.Countries, event.Category)
				if event.Segment == "" {
					continue
				}

				if o.addChannel(item, event.Countries, event.Category, event.Segment) {
					value := *item["value"].N
					fmt.Printf("value: %v\n", value)
					values += pure.StrToFloat64(value)
					fmt.Printf("values: %v\n", values)
					units += pure.StrToFloat64(*item["units"].N)
					prices = append(prices, pure.StrToFloat64(*item["price"].N))
				}

				if event.Channel == "" {
					continue
				}

				o.addSpecValue(item, event.Countries, event.Category, event.Segment, event.Channel)
			}
			return true
		},
	)

	if err != nil {
		panic(err)
	}

	if len(prices) > 0 {
		fmt.Printf("values: %v, units: %v\n", values, units)
		averagePricePerUnit := values / units
		fmt.Printf("averagePricePerUnit: %v\n", averagePricePerUnit)

		min, max := math.MaxFloat64, 0.0
		for _, price := range prices {
			if price < min {
				min = price
			}
			if price > max {
				max = price
			}
		}

		fmt.Printf("min price: %v, max price: %v\n", min, max)

		minPercent, maxPercent := min/averagePricePerUnit*100, max/averagePricePerUnit*100
		fmt.Printf("minPercent: %v, maxPercent: %v\n", minPercent, maxPercent)
		o.PriceIndexRange = priceIndexRange{
			uint16(minPercent),
			uint16(maxPercent),
			averagePricePerUnit / 100,
		}
		fmt.Printf("PriceIndexRange: %#v\n", o.PriceIndexRange)
	}

	return o.transcode(), nil
}
