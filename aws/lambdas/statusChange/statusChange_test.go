package main

import (
	"fmt"
	"testing"

	"../common/mydynamo"
	"../common/pure"
)

const (
	email  = "vitaly@gms-ai.com"
	owner  = "vitaly"
	email2 = "aaa.bbb@gms-ai.com"
	owner2 = "aaa.bbb"

	name      = "go test"
	startDate = "2020-04-20"
	dueDate   = "2022-12-31"

	statusAr            = "Archived"
	statusBeforeArchive = "In Progress"
	statusCom           = "Completed"
)

func init() {
	mydynamo.ProjectDelete(email, name)
	mydynamo.ProjectDelete(email2, name)
}

func TestArchive(t *testing.T) {
	t.Run("owner", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)

		archive(email, owner, name, statusBeforeArchive)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		if *item["entityStatus"].S != "Archived" {
			t.Errorf("entityStatus: " + *item["entityStatus"].S)
		}
		mydynamo.ProjectDelete(email, name)
	})
	t.Run("shareEdit", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), owner2, "edit", false)

		archive(email2, owner, name, statusBeforeArchive)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != "Archived" {
			t.Fatal()
		}
		mydynamo.ProjectDelete(email, name)
	})
	t.Run("shareEditOwnerWithDot", func(t *testing.T) {
		mydynamo.AddProject(email2, name, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email2, name), owner, "edit", false)

		archive(email, owner2, name, statusBeforeArchive)
		item := mydynamo.GetProject(pure.BuildSortKey(email2, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != "Archived" {
			t.Fatal()
		}
		mydynamo.ProjectDelete(email2, name)
	})
	t.Run("shareView", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), owner2, "view", false)

		archive(email2, owner, name, statusBeforeArchive)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S == "Archived" {
			t.Fatal()
		}
		mydynamo.ProjectDelete(email, name)
	})
	t.Run("notShared", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)

		archive(email2, owner, name, statusBeforeArchive)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S == "Archived" {
			t.Fatal()
		}
		mydynamo.ProjectDelete(email, name)
	})
	t.Run("itemNotExists", func(t *testing.T) {
		archive(email, owner, name, statusBeforeArchive)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if item != nil {
			t.Fatal()
		}
	})
}

func TestRestore(t *testing.T) {
	t.Run("owner", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		archive(email, owner, name, statusBeforeArchive)

		restore(email, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusBeforeArchive {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
	t.Run("shareEdit", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		archive(email, owner, name, statusBeforeArchive)
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), owner2, "edit", false)

		restore(email2, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusBeforeArchive {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
	t.Run("shareEditOwnerWithDot", func(t *testing.T) {
		mydynamo.AddProject(email2, name, startDate, dueDate)
		archive(email2, owner2, name, statusBeforeArchive)
		mydynamo.ProjectShare(pure.BuildSortKey(email2, name), owner, "edit", false)

		restore(email, owner2, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email2, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusBeforeArchive {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email2, name)
	})

	t.Run("shareView", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		archive(email, owner, name, statusBeforeArchive)
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), owner2, "view", false)

		restore(email2, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusAr {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
	t.Run("notShared", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		archive(email, owner, name, statusBeforeArchive)

		restore(email2, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusAr {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
}

func TestComplete(t *testing.T) {

	t.Run("owner", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)

		complete(email, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusCom {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
	t.Run("shareEdit", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), owner2, "edit", false)

		complete(email2, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusCom {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
	t.Run("shareEditOwnerWithDot", func(t *testing.T) {
		mydynamo.AddProject(email2, name, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email2, name), owner, "edit", false)

		complete(email, owner2, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email2, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S != statusCom {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email2, name)
	})
	t.Run("shareView", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)
		mydynamo.ProjectShare(pure.BuildSortKey(email, name), owner2, "view", false)

		complete(email2, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S == statusCom {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
	t.Run("notShared", func(t *testing.T) {
		mydynamo.AddProject(email, name, startDate, dueDate)

		complete(email2, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if *item["entityStatus"].S == statusCom {
			t.Fatal()
		}

		mydynamo.ProjectDelete(email, name)
	})
	t.Run("itemNotExists", func(t *testing.T) {
		complete(email, owner, name)
		item := mydynamo.GetProject(pure.BuildSortKey(email, name))
		fmt.Printf("TEST item: %#v\n", item)
		if item != nil {
			t.Fatal()
		}
	})

}
