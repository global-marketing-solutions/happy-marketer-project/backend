package main

import (
	"context"
	"fmt"
	"strings"

	"../common/mydynamo"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	tableName = "marketegy"
)

var (
	updateItemInput = dynamodb.UpdateItemInput{
		TableName: aws.String(tableName),
		Key:       map[string]*dynamodb.AttributeValue{},
	}

	getItemInput = &dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key:       map[string]*dynamodb.AttributeValue{},
	}

	putItemInput = &dynamodb.PutItemInput{
		TableName: aws.String(tableName),
	}

	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("%#v\n", r)
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	owner := r.QueryStringParameters["owner"]
	nameEscaped := pure.DecodePercent(r.PathParameters["name"])

	switch r.QueryStringParameters["status"] {
	case "Archived":
		updateItemInput.Key["pk"] = &dynamodb.AttributeValue{
			S: aws.String(extractType(r.Path)),
		}

		archive(
			email,
			owner,
			nameEscaped,
			r.QueryStringParameters["ex"],
		)
	case "Restore":
		getItemInput.Key["pk"] = &dynamodb.AttributeValue{
			S: aws.String(extractType(r.Path)),
		}

		restore(extractType(r.Path), email, owner, nameEscaped)
	case "Completed":
		mydynamo.Complete(
			email,
			owner,
			nameEscaped,
		)
	default:
		panic("Unrecognized status: " + r.QueryStringParameters["status"])
	}
	return resp, nil
}

func archive(email, owner, name, statusBeforeArchive string) {
	sk := pure.BuildSortKeyForOwner(email, owner, name)
	updateItemInput.Key["sk"] = &dynamodb.AttributeValue{
		S: &sk,
	}
	updateItemInput.UpdateExpression = aws.String("SET entityStatus = :Archived, statusBeforeArchive = :ex")
	updateItemInput.ExpressionAttributeValues = map[string]*dynamodb.AttributeValue{
		":Archived": {
			S: aws.String("Archived"),
		},
		":ex": {
			S: &statusBeforeArchive,
		},
		":sk": {
			S: aws.String(pure.BuildSortKey(email, name)),
		},
		":mode": {
			S: aws.String("edit"),
		},
	}
	updateItemInput.ConditionExpression = aws.String(mydynamo.ConditionExpression())
	updateItemInput.ExpressionAttributeNames = map[string]*string{
		"#username": aws.String(pure.ExtractUsername(email)),
	}
	_, err := mydynamo.DynamodbClient.UpdateItem(&updateItemInput)
	fmt.Printf("archive() updateItemInput: %#v\n", updateItemInput)
	if err != nil {
		mydynamo.ErrCheck(err)
	}
}

func restore(_type, email, owner, name string) {
	item := getEntity(_type, pure.BuildSortKeyForOwner(email, owner, name))
	if !mydynamo.HasEditAccess(pure.BuildSortKey(email, name), item) {
		fmt.Println("HACKING attempt")
		return
	}
	item = transformItem(item)
	replaceItem(item)
}

func transformItem(item map[string]*dynamodb.AttributeValue) map[string]*dynamodb.AttributeValue {
	item["entityStatus"].S = item["statusBeforeArchive"].S
	delete(item, "statusBeforeArchive")
	return item
}
func replaceItem(item map[string]*dynamodb.AttributeValue) {
	putItemInput.SetItem(item)
	_, err := mydynamo.DynamodbClient.PutItem(putItemInput)
	if err != nil {
		panic(err)
	}
}

func extractType(path string) string {
	if strings.HasPrefix(path, "/projects/") {
		return "project"
	}
	return "document"
}

func getEntity(_type, sk string) map[string]*dynamodb.AttributeValue {
	getItemInput := dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String(_type),
			},
			"sk": {
				S: &sk,
			},
		},
	}
	getItemOutput, err := mydynamo.DynamodbClient.GetItem(&getItemInput)
	if err != nil {
		panic(err)
	}
	return getItemOutput.Item
}
