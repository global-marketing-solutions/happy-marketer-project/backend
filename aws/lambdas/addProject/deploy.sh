GOARCH=amd64 GOGC=off go build -ldflags "-s -w"
# ldflags for smaller file, see https://stackoverflow.com/questions/3861634

if [ $? -ne 0 ]; then
    exit
fi

NAME=addProject

rm -f /tmp/$NAME.zip
zip -r /tmp/$NAME.zip $NAME
rm $NAME

AWS_PROFILE=gms-ai \
aws lambda update-function-code --function-name=$NAME --zip-file=fileb:///tmp/$NAME.zip
