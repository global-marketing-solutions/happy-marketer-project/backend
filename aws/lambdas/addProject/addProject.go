package main

import (
	"fmt"

	"../common/mydynamo"

	"github.com/aws/aws-lambda-go/lambda"
)

type event struct {
	Email     string
	Name      string
	StartDate string
	DueDate   string
}

func main() {
	lambda.Start(handleRequest)
}

func handleRequest(event event) error {
	fmt.Printf("%#v\n", event)
	mydynamo.AddProject(event.Email, event.Name, event.StartDate, event.DueDate)

	return nil
}
