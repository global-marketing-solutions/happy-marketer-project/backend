package main

import (
	"context"
	"encoding/json"

	"../common/isAvailable"
	"../common/mycognito"
	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

var (
	sess         = session.Must(session.NewSession())
	dynamoClient = dynamodb.New(sess)

	// https://docs.aws.amazon.com/sdk-for-go/api/service/dynamodb/#QueryInput
	queryInput = &dynamodb.QueryInput{
		TableName: aws.String("marketegy"),
	}

	keyCondBuilder expression.KeyConditionBuilder

	condBuilder expression.ConditionBuilder

	expr expression.Expression

	loginsToHumanNamesCache map[string]string

	resp = events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}
)

type document struct {
	Name string

	OwnerUsername string
	OwnerName     string

	Shrd      map[string]map[string]string `json:",omitempty"`
	StartDate string
	DueDate   string

	Params *struct {
		Brand, Currency, TypeOfPrice string
		Specs                        []struct {
			Name   string
			Values []string
		}
		SpecsPercentsX []uint
		SpecsPercentsY []uint
	}

	Models []*struct {
		Id    string
		Price uint16
		Specs map[string]string
	} `json:",omitempty"`
}

func init() {
	mycognito.InitWithSession(sess)
}

func main() {
	lambda.Start(handle)
}
func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	status := pure.DecodePercent(r.QueryStringParameters["status"])

	keyCondBuilder = expression.Key("pk").Equal(expression.Value("document")).And(
		expression.Key("sk").BeginsWith(pure.ExtractDomain(email) + " "))

	if status == "Archived" {
		condBuilder = expression.Name("entityStatus").Equal(expression.Value("Archived"))
	} else {
		condBuilder = expression.Name("entityStatus").Equal(expression.Value("In Progress"))
	}

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondBuilder).WithFilter(condBuilder).Build()
	if err != nil {
		panic(err)
	}

	queryInput.KeyConditionExpression = expr.KeyCondition()
	queryInput.ExpressionAttributeNames = expr.Names()
	queryInput.ExpressionAttributeValues = expr.Values()
	queryInput.FilterExpression = expr.Filter()

	queryOutput, err := dynamoClient.Query(queryInput)
	if err != nil {
		panic(err)
	}

	documents := []document{}
	for _, item := range queryOutput.Items {
		if isAvailable.IsAccessMatch(email, *item["sk"].S, item["shrd"]) {
			d := document{
				Name: pure.ProjectName(*item["sk"].S),

				OwnerUsername: pure.Username(*item["sk"].S),
				OwnerName:     mycognito.GetHumanName(pure.Email(*item["sk"].S)),

				StartDate: *item["startDate"].S,
				DueDate:   *item["dueDate"].S,
			}
			if item["shrd"] != nil {
				shrd := make(map[string]string)
				dynamodbattribute.Unmarshal(item["shrd"], &shrd)
				d.Shrd = make(map[string]map[string]string)
				for login, mode := range shrd {
					d.Shrd[login] = map[string]string{
						"humanName": mycognito.GetHumanName(login + "@" + pure.ExtractDomain(email)),
						"mode":      mode,
					}
				}
			}

			if item["params"] != nil {
				dynamodbattribute.Unmarshal(item["params"], &d.Params)
			}

			if item["models"] != nil {
				dynamodbattribute.Unmarshal(item["models"], &d.Models)
			}

			documents = append(documents, d)
		}

	}

	b, err := json.Marshal(documents)
	if err != nil {
		panic(err)
	}

	resp.Body = string(b)

	return resp, nil
}
