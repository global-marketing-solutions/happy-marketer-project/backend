package main

import (
	"context"

	"../common/pure"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	Sess           = session.Must(session.NewSession())
	DynamodbClient = dynamodb.New(Sess)
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	email := r.RequestContext.Authorizer["claims"].(map[string]interface{})["email"].(string)
	projectName := pure.DecodePercent(r.PathParameters["name"])
	ownerUsername := r.QueryStringParameters["ownerUsername"]

	if ownerUsername == "" {
		DocumentDelete(email, projectName)
	} else {
		sk := pure.BuildSortKey(ownerUsername+"@"+pure.ExtractDomain(email), projectName)
		DocumentUnshare(sk, pure.ExtractUsername(email))
	}

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}, nil
}

func DocumentDelete(email, name string) {
	deleteItemInput := dynamodb.DeleteItemInput{
		TableName: aws.String("marketegy"),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("document"),
			},
			"sk": {
				S: aws.String(pure.BuildSortKey(email, name)),
			},
		},
	}
	_, err := DynamodbClient.DeleteItem(&deleteItemInput)
	if err != nil {
		panic(err)
	}
}

func DocumentUnshare(sk, userNameUnshare string) {
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName: aws.String("marketegy"),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("document"),
			},
			"sk": {
				S: &sk,
			},
		},
		UpdateExpression: aws.String("REMOVE shrd.#0"),
		ExpressionAttributeNames: map[string]*string{
			"#0": &userNameUnshare,
		},
	}

	_, err := DynamodbClient.UpdateItem(updateItemInput)
	if err != nil {
		panic(err)
	}
}
