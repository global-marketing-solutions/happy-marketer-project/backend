package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"io"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	bucketName = "marketegy--avatars"
)

var (
	sess = session.Must(session.NewSession())
	svc  = s3.New(sess)
)

func main() {
	lambda.Start(handle)
}

func handle(ctx context.Context, r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("r.Headers: %#v, r.Body: %#v\n", r.Headers, r.Body)

	sub := r.RequestContext.Authorizer["claims"].(map[string]interface{})["sub"].(string)
	contentType := r.Headers["content-type"]

	img, err := base64.StdEncoding.DecodeString(r.Body)
	if err != nil {
		panic(err)
	}

	upload(bucketName, sub, contentType, bytes.NewReader(img))

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "https://marketegy.gms-ai.com",
		},
	}, nil
}

func upload(bucketName, sub, contentType string, img io.ReadSeeker) {
	// https://docs.aws.amazon.com/sdk-for-go/api/service/s3/#S3.PutObject
	out, err := svc.PutObject(&s3.PutObjectInput{
		Bucket:      &bucketName,
		ACL:         aws.String("public-read"),
		Key:         &sub,
		ContentType: &contentType,
		Body:        img,
	})
	if err != nil {
		panic(err)
	}
	fmt.Printf("out: %#v\n", out)
}
