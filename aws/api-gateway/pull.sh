AWS_PROFILE=gms-ai \
    aws apigateway get-export \
        --rest-api-id 5y7lkvtie6 \
        --stage-name prod \
        --export-type swagger \
        --parameters '{"extensions": "integrations,authorizers"}' \
        --accepts application/yaml \
        swagger.yaml
