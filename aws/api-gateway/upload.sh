# This is not a deploy, for prod delivery - deploy through the web UI or API

AWS_PROFILE=gms-ai \
    aws apigateway put-rest-api \
        --rest-api-id '5y7lkvtie6' \
        --body 'file://swagger.yaml'
